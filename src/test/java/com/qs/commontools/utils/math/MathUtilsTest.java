package com.qs.commontools.utils.math;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

/**
 * 数学工具类 单元测试
 * @author qiusuo
 * @since 2021-10-25
 */
@SpringBootTest
public class MathUtilsTest {

    /**
     * todo 将整数化为千分位分割字符串
     */
    @Test
    void micrometer() {
        System.out.println(MathUtils.micrometer(100000000000000L));
    }

    /**
     * todo 百分比
     */
    @Test
    void percent() {
        float y = MathUtils.percent(17, 76,2);
        System.out.println(y);
    }

    @Test
    void sw() {
        System.out.println((float)0/0);
        System.out.println(BigDecimal.valueOf(0).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
    }
}
