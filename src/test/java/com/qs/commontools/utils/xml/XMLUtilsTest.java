package com.qs.commontools.utils.xml;

import com.qs.commontools.utils.base.SystemUtils;
import org.dom4j.DocumentException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

/**
 * XML工具类单元测试
 * @author qiusuo
 * @since 2021-09-11
 */
@SpringBootTest
public class XMLUtilsTest {

    /**
     * 字符串类型XML解析
     */
    @Test
    void StringXMLToMap() throws DocumentException {
        String XMLString = "<License><BusinessInfo><Field CName=\"传真\" value=\"\"/><Field CName=\"备注\" value=\"\"/><Field CName=\"单位名称\" value=\"1\"/><Field CName=\"成立时间\" value=\"2021-09-10\"/><Field CName=\"主营业务\" value=\"1\"/><Field CName=\"主要作品\" value=\"1\"/><Field CName=\"业务范围\" value=\"1\"/><ChildBusinessInfo></ChildBusinessInfo></BusinessInfo></License>";
//        String XMLString = "<sites><site><name key=\"name\">RUNOOB</name><url>www.runoob.com</url></site><site><name phone=\"13814602018\">Google</name><url>www.google.com</url></site><site><name>Facebook</name><url>www.facebook.com</url></site><site></site></sites><sitess><site>www.ddd.com</site></sitess>";
//        String XMLString = "<sites a1=\"a1\" a2=\"a2\" a3=\"a3\">aadsd <site><name key=\"name\">RUNOOB</name><url>www.runoob.com</url></site><site><name phone=\"13814602018\">Google</name><url>www.google.com</url><sit a1=\"a1\" a2=\"a2\"><si>\n" +
//                "张三</si></sit></site><site><name>Facebook</name><url>www.facebook.com</url></site><site>李四</site>为我</sites>";
        XMLTool parse = XMLTool.parse(XMLString);
        System.out.println(parse.toString());
    }


    /**
     * xml文件解析
     */
    @Test
    void parseFile() throws DocumentException {
        File file = new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "file" + File.separator + "test.xml");
        XMLTool parse = XMLTool.parse(file);
        System.out.println(parse.toString());
    }
}
