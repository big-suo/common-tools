package com.qs.commontools.utils.cache;

import com.qs.commontools.common.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

/**
 * redis测试类
 * @author qiusuo
 * @since 2021-10-08
 */
@SpringBootTest
public class RedisTest {
    // todo 通用

    /**
     * 指定缓存失效时间
     */
    @Test
    void expire() {
        System.out.println(RedisUtils.expire("pwd",10000));
    }

    /**
     *  根据key 获取过期时间
     */
    @Test
    void getExpire() {
        System.out.println(RedisUtils.getExpire("pwd"));
    }

    /**
     * 判断key是否存在
     */
    @Test
    void hasKey() {
        System.out.println(RedisUtils.hasKey("pwd"));
        System.out.println(RedisUtils.hasKey("pwd1"));
    }

    /**
     * 删除缓存
     */
    @Test
    void del() {
        RedisUtils.del("pwd","user1");
    }

    // todo String

    /**
     * String类型普通缓存放入
     */
    @Test
    void set() {
        boolean set = RedisUtils.set("user", "zhansan");
        User user = new User();
        user.setUsername("王五");
        user.setPwd("12345");
        user.setFile(new File("C:\\qiusuo"));
        boolean user1 = RedisUtils.set("user1", user, 100000);
        boolean pwd = RedisUtils.set("pwd", 12454);
        System.out.println(user1);
        System.out.println(set);

    }

    /**
     * String类型获得
     */
    @Test
    void get() {
        User user = (User)RedisUtils.get("user1");
        System.out.println(user.toString());
    }
}
