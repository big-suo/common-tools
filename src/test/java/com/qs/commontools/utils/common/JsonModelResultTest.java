package com.qs.commontools.utils.common;

import com.alibaba.fastjson.JSON;
import com.qs.commontools.common.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Json类型结果模版单元测试
 * @author qiusuo
 * @since 2021-11-25
 * @see JsonModelResult
 */
@SpringBootTest
public class JsonModelResultTest {

    @Test
    void test() {
        JsonModelResult instance = JsonModelResult.getInstance();
        instance.setMsg("请求成功");
        instance.setCode(200);
        instance.setSuccess(true);
        instance.setData(new User());
        System.out.println(JSON.toJSONString(instance));
    }
}
