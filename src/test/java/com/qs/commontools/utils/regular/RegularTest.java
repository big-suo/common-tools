package com.qs.commontools.utils.regular;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 正则单元测试
 * @author qiusuo
 * @since 2021-09-30
 */
@SpringBootTest
public class RegularTest {

    /**
     * todo 校验年
     * @see RegularUtils#checkYear(String)
     */
    @Test
    void checkYear(){
        boolean b = RegularUtils.checkYear("2018");
        System.out.println(b);
    }

    /**
     * todo 检验正整数
     * @see RegularUtils#checkNumPositiveInteger(String)
     */
    @Test
    void checkNumPositiveInteger() {
        System.out.println(RegularUtils.checkNumPositiveInteger("-1"));    // false
        System.out.println(RegularUtils.checkNumPositiveInteger("0"));     // false
        System.out.println(RegularUtils.checkNumPositiveInteger("102"));   // true
        System.out.println(RegularUtils.checkNumPositiveInteger("102.0")); // false
        System.out.println(RegularUtils.checkNumPositiveInteger("213d"));  // false
    }

    /**
     * todo 检验大于等于0的整数
     * @see RegularUtils#checkNumPositiveZeroInt(String)
     */
    @Test
    void checkNumPositiveZeroInt() {
        System.out.println(RegularUtils.checkNumPositiveZeroInt("-1"));  // false
        System.out.println(RegularUtils.checkNumPositiveZeroInt("-1.2"));  //false
        System.out.println(RegularUtils.checkNumPositiveZeroInt("0")); // true
        System.out.println(RegularUtils.checkNumPositiveZeroInt("102"));   //true
        System.out.println(RegularUtils.checkNumPositiveZeroInt("102.2")); //false
        System.out.println(RegularUtils.checkNumPositiveZeroInt("213d"));  // false
    }

    /**
     * todo 检验是否是数字
     * @see RegularUtils#checkNum(String)
     * @see RegularUtils#checkNumInteger(String, Integer)
     * @see RegularUtils#checkNumIntAtLeast(String, Integer)
     * @see RegularUtils#checkNumIntRange(String, Integer, Integer)
     */
    @Test
    void checkNum() {
        String i = null;
        System.out.println(RegularUtils.checkNum("3.2"));       // true
        System.out.println(RegularUtils.checkNum("3"));         // true
        System.out.println(RegularUtils.checkNum("-3"));        // true
        System.out.println(RegularUtils.checkNum("-3.2"));      // true
        System.out.println(RegularUtils.checkNum("-3。2"));     // false
        System.out.println(RegularUtils.checkNum("0"));         // true
        System.out.println(RegularUtils.checkNum("012"));       // true
        System.out.println(RegularUtils.checkNum("122"));       // true
        System.out.println(RegularUtils.checkNum("122d"));      // false
        System.out.println(RegularUtils.checkNum(i));               // false
        // 判断是否是n位的数字字符串
        System.out.println(RegularUtils.checkNumInteger("0123",4));
        // 判断至少几位数字字符串
        System.out.println(RegularUtils.checkNumIntAtLeast("012",1));
        // 判断是否m-n位的数字字符串
        System.out.println(RegularUtils.checkNumIntRange("012",1,2));
    }

    /**
     * 校验浮点数
     * @see RegularUtils#checkNumFloat(String)
     * @see RegularUtils#checkNumFloatPositiveZero(String)
     * @see RegularUtils#checkNumFloatNegativeZero(String)
     * @see RegularUtils#checkNumFloatPositive(String)
     * @see RegularUtils#checkNumFloatNegative(String)
     * @see RegularUtils#checkNumFloatRange(String, Integer, Integer)
     *
     */
    @Test
    void checkFloat() {
        // 浮点数
        System.out.println(RegularUtils.checkNumFloat("3.2"));    //true
        System.out.println(RegularUtils.checkNumFloat("-3.2"));   //true
        System.out.println(RegularUtils.checkNumFloat("-3"));       //false
        System.out.println(RegularUtils.checkNumFloat("0"));       //true
        System.out.println(RegularUtils.checkNumFloat("0.0"));       //true
        System.out.println(RegularUtils.checkNumFloat("3"));  //false
        System.out.println(RegularUtils.checkNumFloat("d"));  //false

        System.out.println("---------------------------------------------");
        //非负浮点数
        System.out.println(RegularUtils.checkNumFloatPositiveZero("3.2"));  //true
        System.out.println(RegularUtils.checkNumFloatPositiveZero("-3.2")); //false
        System.out.println(RegularUtils.checkNumFloatPositiveZero("-3")); //false
        System.out.println(RegularUtils.checkNumFloatPositiveZero("0")); //true
        System.out.println(RegularUtils.checkNumFloatPositiveZero("0.0")); //true
        System.out.println(RegularUtils.checkNumFloatPositiveZero("3")); //false
        System.out.println(RegularUtils.checkNumFloatPositiveZero("d")); //false

        System.out.println("---------------------------------------------");

        //非正浮点数
        System.out.println(RegularUtils.checkNumFloatNegativeZero("3.32")); //false
        System.out.println(RegularUtils.checkNumFloatNegativeZero("-3.2")); //true
        System.out.println(RegularUtils.checkNumFloatNegativeZero("0")); //true
        System.out.println(RegularUtils.checkNumFloatNegativeZero("0.0")); //true
        System.out.println(RegularUtils.checkNumFloatNegativeZero("1")); //false
        System.out.println(RegularUtils.checkNumFloatNegativeZero("-1")); //false
        System.out.println(RegularUtils.checkNumFloatNegativeZero("d")); //false

        System.out.println("---------------------------------------------");

        // 正浮点数
        System.out.println(RegularUtils.checkNumFloatPositive("3.2")); //true
        System.out.println(RegularUtils.checkNumFloatPositive("-3.2")); //false
        System.out.println(RegularUtils.checkNumFloatPositive("0"));    //false
        System.out.println(RegularUtils.checkNumFloatPositive("0.0"));    //false
        System.out.println(RegularUtils.checkNumFloatPositive("-1"));   //false
        System.out.println(RegularUtils.checkNumFloatPositive("1"));    //false
        System.out.println(RegularUtils.checkNumFloatPositive("d"));    //false

        System.out.println("---------------------------------------------");

        // 负浮点数
        System.out.println(RegularUtils.checkNumFloatNegative("3.2"));    //false
        System.out.println(RegularUtils.checkNumFloatNegative("-3.2"));    //true
        System.out.println(RegularUtils.checkNumFloatNegative("0"));    //false
        System.out.println(RegularUtils.checkNumFloatNegative("0.0"));    //false
        System.out.println(RegularUtils.checkNumFloatNegative("1"));    //false
        System.out.println(RegularUtils.checkNumFloatNegative("-1"));    //false
        System.out.println(RegularUtils.checkNumFloatNegative("d"));    //false

        System.out.println("---------------------------------------------");

        // 判断是否带m-n位小数的正数或负数字符串。
        System.out.println(RegularUtils.checkNumFloatRange("3.1",2,4));    //false
        System.out.println(RegularUtils.checkNumFloatRange("3.1",1,2));    //true
        System.out.println(RegularUtils.checkNumFloatRange("3.12",1,2));    //true
        System.out.println(RegularUtils.checkNumFloatRange("3.12",1,1));    //false
        System.out.println(RegularUtils.checkNumFloatRange("3.12",2,2));    //true
        System.out.println(RegularUtils.checkNumFloatRange("3.123",1,2));    //false
    }

    /**
     * todo 检查是否是整数
     * @see RegularUtils#checkNumInteger(String)
     */
    @Test
    void checkNumInteger() {
        System.out.println(RegularUtils.checkNumInteger("-1.3")); //false
        System.out.println(RegularUtils.checkNumInteger("-1")); //true
        System.out.println(RegularUtils.checkNumInteger("0")); //true
        System.out.println(RegularUtils.checkNumInteger("3")); //true
        System.out.println(RegularUtils.checkNumInteger("3.1")); //false
        System.out.println(RegularUtils.checkNumInteger("3.1d")); //false
        System.out.println(RegularUtils.checkNumInteger("3L")); //false
    }

    /**
     * todo 校验香港手机号码
     * @see RegularUtils#checkPhoneHK(String)
     */
    @Test
    void checkPhoneHK() {
        System.out.println(RegularUtils.checkPhoneHK("48274627")); // false
        System.out.println(RegularUtils.checkPhoneHK("58274627")); // true
    }

    /**
     * todo 校验大陆手机号码
     * @see RegularUtils#checkPhone(String)
     */
    @Test
    void checkPhone() {
        System.out.println(RegularUtils.checkPhone("13817892987"));
        System.out.println(RegularUtils.checkPhone("138178929871"));
        System.out.println(RegularUtils.checkPhone("1381789271"));
    }

    /**
     * todo 校验邮箱
     * @see RegularUtils#checkEmail(String)
     */
    @Test
    void checkEmail() {
        System.out.println(RegularUtils.checkEmail("123@126.com"));
    }

    /**
     * todo 校验身份证相关
     * @see RegularUtils#checkIdCard(String)
     * @see RegularUtils#checkIdCard18(String)
     * @see RegularUtils#checkIdCard15(String)
     * @see RegularUtils#checkIdCardSuffix6(String)
     */
    @Test
    void checkIdcard() {
        System.out.println(RegularUtils.checkIdCard("632323190605265264"));
        System.out.println(RegularUtils.checkIdCard15("632323190605265264"));
        System.out.println(RegularUtils.checkIdCard18("632323190605265264"));
    }

    /**
     * todo 判断是否是汉字
     * @see RegularUtils#checkZhCn(String)
     */
    @Test
    void checkZhCn() {
        System.out.println(RegularUtils.checkZhCn("hazi安徽"));
        System.out.println(RegularUtils.checkZhCn("阿沙发客"));
    }

    /**
     * todo ip
     * @see RegularUtils#checkIP(String)
     */
    @Test
    void checkIP() {
        System.out.println(RegularUtils.checkIP("255,255,255,0"));
        System.out.println(RegularUtils.checkIP("255.255.255.0"));
        System.out.println(RegularUtils.checkIP("192.168.1.12"));
    }

    /**
     * todo html
     * @see RegularUtils#checkTag(String)
     */
    @Test
    void checkTag() {
        System.out.println(RegularUtils.checkTag("<a>"));
        System.out.println(RegularUtils.checkTag("<d>"));
        System.out.println(RegularUtils.checkTag("<a></a>"));
    }

    /**
     * todo URL
     * @see RegularUtils#checkHttpURL(String)
     */
    @Test
    void checkURL() {
        System.out.println(RegularUtils.checkHttpURL("https://www.baidu.com/s?wd=java%20%E6%AD%A3%E5%88%99%E8%A1%A8%E8%BE%BE%E5%BC%8F&rsv_spt=1&rsv_iqid=0xf233885e000326c0&issp=1&f=8&rsv_bp=0&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_enter=1&rsv_sug3=24&rsv_sug1=8&rsv_sug7=100&rsv_t=0d31XJ5IR0T98Bv150wUMKQHirYYsh2IgKsJFk0FH4wGur10ND3LypRnWtdrcFCsDH%2F3&rsv_sug2=0&inputT=6942&rsv_sug4=6942"));
        System.out.println(RegularUtils.checkHttpURL("https://www.baidu.com"));
        System.out.println(RegularUtils.checkHttpURL("https://127.0.0.1"));
        System.out.println(RegularUtils.checkHttpURL("https://127.0.0.1:8080"));
        System.out.println(RegularUtils.checkHttpURL("ws://127.0.0.1:8080"));
    }

}
