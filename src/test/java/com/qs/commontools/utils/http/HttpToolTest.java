package com.qs.commontools.utils.http;

import com.qs.commontools.common.entity.User;
import com.qs.commontools.constants.HttpToolConstant;
import com.qs.commontools.utils.base.Base64StringTool;
import com.qs.commontools.utils.base.SystemUtils;
import com.qs.commontools.utils.file.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * httpTool工具单元测试类
 * @author qiusuo
 * @since 2021-11-24
 */
@SpringBootTest
public class HttpToolTest {
    /**
     * todo get发送无参请求
     */
    @Test
    void get() {
        try {
            HttpTool tool = HttpTool.sendGet("http://localhost:8080/test/u3");
            System.out.println(tool.getResult());
            System.out.println(tool.getCode());
            System.out.println(tool.getMsg());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * todo get发送有参请求，直接拼接
     */
    @Test
    void getParam1() {
        try {
            HttpTool tool = HttpTool.sendGet("http://localhost:8080/test/u4?username=张三&pwd=admin@123");
            System.out.println(tool.send());
            System.out.println(tool.getCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * todo get发送有参请求，map入参
     */
    @Test
    void getParam2() {
        try {
            Map param = new HashMap();
            param.put("username", "张三");
            param.put("pwd", "admin@123");
            HttpTool tool = HttpTool.sendGet("http://localhost:8080/test/u4",param);
            System.out.println(tool.getResult());
            System.out.println(tool.getCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * todo post表单传输
     */
    @Test
    void post1() {
        try {
            // 封装入参
            Map<String,Object> param = new HashMap();
            param.put("username", "张三");
            param.put("pwd","admin@123");
            // 传附件必须要创建绑定File对象
            param.put("file", new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                    "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                    "commontools" + File.separator + "common" + File.separator + "file" + File.separator + "test.xml"));
            param.put("file1", new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                    "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                    "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "slf4j.jpg"));
            HttpTool tool = HttpTool.sendPost("http://localhost:8080/test/u2",param,false);
            System.out.println(tool.getResult());
            System.out.println(tool.getCode());
            // 设置入参
            Map<String,Object> param2 = new HashMap<>(HttpToolConstant.NUM_INTEGER_SIXTEEN);
            param2.put("username", "张三");
            param2.put("pwd", "admin@123");
            HttpTool tool2 = HttpTool.sendPost("http://localhost:8080/test/u8",param2,false);
            System.out.println(tool2.getResult());
            System.out.println(tool2.getCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * todo post JSON传输
     */
    @Test
    void post2() {
        try {
            Map<String,Object> param = new HashMap();
            param.put("username", "张三");
            param.put("pwd","admin@123");
            HttpTool tool = HttpTool.sendPost("http://localhost:8080/test/u5",param,true);
            System.out.println(tool.getResult());
            System.out.println(tool.getCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * todo send发送无参get请求
     */
    @Test
    void sendGet() {
        try {
            HttpTool tool = HttpTool.getInstance("http://localhost:8080/test/u3");
            String send = tool.send();
            System.out.println(send);
            System.out.println(tool.getCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * todo send发送有参get表单请求,直接拼接在路径里
     */
    @Test
    void sendGetFormParam1() throws Exception {
        HttpTool tool = HttpTool.getInstance("http://localhost:8080/test/u4?username=张三&pwd=admin@123");
        tool.send();
        System.out.println(tool.getResult());
        System.out.println(tool.getCode());

    }

    /**
     * todo send 发送有参get表单请求，Map入参
     */
    @Test
    void sendGetFormParam2() throws Exception {
        HttpTool tool = HttpTool.getInstance("http://localhost:8080/test/u4");
        Map param = new HashMap();
        param.put("username", "张三");
        param.put("pwd", "admin@123");
        tool.setParam(param);
        tool.send();
        System.out.println(tool.getResult());
        System.out.println(tool.getCode());
    }

    /**
     * todo send 发送有参get表单请求，String入参
     */
    @Test
    void sendGetFormParam3() throws Exception {
        HttpTool tool = HttpTool.getInstance("http://localhost:8080/test/u4");
        tool.setParam("username=张三&pwd=admin@123");
        tool.send();
        System.out.println(tool.getResult());
        System.out.println(tool.getCode());
    }

    /**
     * todo send 发送有参post（Json）请求
     */
    @Test
    void sendPostJsonParam() throws Exception {
        HttpTool tool = HttpTool.getInstance("http://localhost:8080/test/u5");
        // 设置POST请求
        tool.setRequestMethod(HttpToolConstant.REQUEST_METHOD_POST);
        // 设置json请求
        tool.setContentType(HttpToolConstant.CONTENT_TYPE_APPLICATION_JSON);
        // 设置入参
        Map<String,Object> param = new HashMap<>(HttpToolConstant.NUM_INTEGER_SIXTEEN);
        param.put("username", "张三");
        param.put("age", 12);
        param.put("boolean", true);
        param.put("user",new User());
        tool.setParam(param);
        tool.send();
        System.out.println(tool.getResult());
        System.out.println(tool.getCode());
    }

    /**
     * todo send发送有参post（表单）请求
     */
    @Test
    void sendPostFormParam() throws Exception {
        HttpTool tool = HttpTool.getInstance("http://localhost:8080/test/u8");
        // 设置POST请求
        tool.setRequestMethod(HttpToolConstant.REQUEST_METHOD_POST);
        // 设置入参
        Map<String,Object> param = new HashMap<>(HttpToolConstant.NUM_INTEGER_SIXTEEN);
        param.put("username", "张三");
        param.put("pwd", "admin@123");
        tool.setParam(param);
        tool.send();
        System.out.println(tool.getResult());
        System.out.println(tool.getCode());
        System.out.println(tool.getMsg());

    }

    /**
     * todo send传不同名附件和其他参数
     */
    @Test
    void sendFileAndString() throws Exception {
        HttpTool httpTool = HttpTool.getInstance("http://localhost:8080/test/u2");
        // 设置请求为post，传输类型不为post会报错
        httpTool.setRequestMethod(HttpToolConstant.REQUEST_METHOD_POST);
        // 封装入参
        Map<String,Object> param = new HashMap();
        param.put("username", "张三");
        param.put("pwd","admin@123");
        // 传附件必须要创建绑定File对象
        param.put("file", new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "file" + File.separator + "test.xml"));
        param.put("file1", new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "slf4j.jpg"));

        // 会自动解析File对象为要传附件
        httpTool.setParam(param);
        httpTool.send();
        System.out.println(httpTool.getResult());
        System.out.println(httpTool.getCode());
        System.out.println(httpTool.getMsg());
    }

    /**
     * todo send传同名附件和其他参数
     */
    @Test
    void sendSameNameFileAndString() throws Exception {
        HttpTool httpTool = HttpTool.getInstance("http://localhost:8080/test/u1");
        // 设置请求为post
        httpTool.setRequestMethod(HttpToolConstant.REQUEST_METHOD_POST);

        // 封装入参
        Map<String,Object> param = new HashMap();
        param.put("username", "张三");
        param.put("pwd","admin@123");
        // 传附件必须要创建绑定File对象
        param.put("file", new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "file" + File.separator + "test.xml"));
        param.put("file1", new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "slf4j.jpg"));

        // 第一个需要入参的Map键值对会自动解析File对象为要传附件
        // 第二个是传输的附件对象的需要设置同名的名称（对应前端的单个上传选择器批量上传，所有的附件对应一个key的情形）
        httpTool.setParam(param,"file");

        // 发送请求
        httpTool.send();
        System.out.println(httpTool.getResult());
        System.out.println(httpTool.getCode());
        System.out.println(httpTool.getMsg());
    }



    /**
     * 阿里巴巴接口测试
     */
    @Test
    void alibaba(){
        HttpTool tool = HttpTool.getInstance("https://api01.aliyun.venuscn.com/ip");
        String appcode = "847e08296b6648ec832e1883a11061bd";
        tool.setContentType(HttpToolConstant.CONTENT_TYPE_FORM_DATA);
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("ip","221.231.219.6");
        tool.setParam(querys);
        tool.setHeader(headers);
        try {
            String send = tool.send();
            System.out.println(send);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void textPlain() {

        HttpTool tool = HttpTool.getInstance("https://wx.lcgjj.com.cn/esb_zhfw_wl/service/wlcx");
        tool.setRequestMethod("POST");
        tool.setContentType(HttpToolConstant.CONTENT_TYPE_TEXT_PLAIN);
        tool.setParam("eaa1959ebfbac74cc9366cfc8fbccb8aH4sIAAAAAAAAAGWOsQ7DIAxEd74C3cxgQFFL/oYWpFA1IS0sDeLf6ybZ6u3unc9umKIPGIXkafDrmljhSlCob7+Uew6RDUNEhjRpKLmDmubD15ou7A9kteGlVOJryX+EWKErgVsOH4xtvyfxTKVyFurQ8vymYXtMMwNryJlBO2d+RVxp0c9oF/0LBKLH7L0AAAA=");
        try {
            System.out.println(tool.send());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void trs() {
        HttpTool tool = HttpTool.getInstance("http://localhost:8080/test/u12");
        tool.setRequestMethod("POST");
        String send = "";
        try {
             send = tool.send();
            byte[] bytes = Base64StringTool.decodeBase64ToByte(send);
            FileUtils.save(bytes,"/Users/qiusuo/Downloads","1","zip");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
