package com.qs.commontools.utils.http;

import com.qs.commontools.utils.base.Base64StringTool;
import com.qs.commontools.utils.file.FileUtils;
import org.apache.http.HttpResponse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

/**
 * http工具类单元测试
 * @author qiusuo
 * @since 2021-09-11
 */
@SpringBootTest
class HttpUtilsTest {


    @Test
    void test1() throws Exception {
        String host = "https://api01.aliyun.venuscn.com";
        String path = "/ip";
        String appcode = "847e08296b6648ec832e1883a11061bd";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("ip","221.231.219.6");
        HttpResponse response = HttpUtils.doGet(host, path, headers,
                querys);
        System.out.println(HttpUtils.getResult(response));
//        HttpTool tool = HttpTool.getInstance("https://api01.aliyun.venuscn.com/ip");
//        Map<String,Object> headers = new HashMap<>();
//        headers.put("Authorization ","APPCODE " +"847e08296b6648ec832e1883a11061bd");
//        tool.setHeader(headers);
//        Map param = new HashMap();
//        param.put("ip","221.231.219.6");
//        tool.setParam(param);
//        try {
//            tool.send();
//            System.out.println(tool.getResult());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Test
    void Test() throws Exception {

        String host = "http://checkone.market.alicloudapi.com";
        String path = "/chinadatapay/1882";
        String method = "POST";
        String appcode = "847e08296b6648ec832e1883a11061bd";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        Map<String, String> querys = new HashMap<String, String>();
        Map<String, String> bodys = new HashMap<String, String>();
        bodys.put("name","王武");
        bodys.put("idcard","32061119850214465X");
        HttpResponse response = HttpUtils.doPost(host, path, headers, querys, bodys);
        System.out.println(HttpUtils.getResult(response));

    }

    @Test
    void trs() {

        try {
            HttpResponse response = HttpUtils.doPost("http://localhost:8080","/test/u12",new HashMap<>(),null,"");
            byte[] bytes = Base64StringTool.decodeBase64ToByte(HttpUtils.getResult(response));
            FileUtils.save(bytes,"/Users/qiusuo/Downloads","1","zip");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
