package com.qs.commontools.utils.file;

import com.qs.commontools.utils.base.StringUtils;
import com.qs.commontools.utils.base.SystemUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * 验证码工具测试
 * @author qiusuo
 * @since 2021-11-16
 */
@SpringBootTest
public class VerificationToolTest {

    /**
     * todo 验证码
     */
    @Test
    void getVerification() throws IOException {
        VerificationTool tool = VerificationTool.getInstance();
        // 设置生成图片的长宽像素
        tool.setHeight(180);
        tool.setWidth(360);
        // 设置绘制的验证码是否随机上下浮动，默认为浮动
        tool.setRandomLocation(true);
        // 默认获取最基础4位验证码
        tool.setCode(StringUtils.getVerification());
        // 默认验证码颜色为黑色
        tool.setCodeColor(Color.BLACK);
        // 默认背景为白色
        tool.setBackgroundColor(new Color(255, 255, 255));
        // 默认干扰线数量为10
        tool.setInterLineNum(10);
        // 默认干扰线颜色灰色
        tool.setInterLineColor(Color.GRAY);
        // 默认干扰线的宽度为10
        tool.setInterLineWeight(10.0f);
        // 默认干扰点点的比例为0.05
        tool.setPointRate(0.05f);
        // 默认干扰点点的颜色
        tool.setPointColor(new Color(56, 51, 51));
        // 默认生成的图片文件后缀为jpg
        tool.setSuffix("jpg");
        byte[] bytes = tool.create();
        String verification = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator ;

        ImgUtils.save(bytes, verification, "verification", "jpg");
    }
}
