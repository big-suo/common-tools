package com.qs.commontools.utils.file;

import com.qs.commontools.utils.base.SystemUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@SpringBootTest
public class ImgUtilsTest {

    /**
     * todo 获取文字图片
     */
    @Test
    void getImg() {
        String text = "元";
        byte[] bytes = ImgUtils.getImg(text);
        try {
            ImgUtils.save(bytes, "C:\\Users\\hanweb\\Desktop\\", text , "jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /**
     * todo InputStream转Byte数组
     */
    @Test
    void inputStreamToByte() {
        InputStream inputStream = null;
        try {
            byte[] bytes = ImgUtils.inputStreamToByte(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * todo Byte数组转InputStream
     */
    @Test
    void byteToInputStream() {
        byte[] bytes = new byte[20];
        InputStream inputStream = ImgUtils.byteToInputStream(bytes);
    }

    /**
     * todo BufferedImage对象转InputStream
     */
    @Test
    void bufferedImageToInputStream() {
        BufferedImage bufferedImage = new BufferedImage(360,120,BufferedImage.TYPE_INT_RGB);
        InputStream inputStream = ImgUtils.bufferedImageToInputStream(bufferedImage, "jpg");
        InputStream inputStream2 = ImgUtils.bufferedImageToInputStream(bufferedImage);
    }

    /**
     * todo BufferedImage对象转Byte数组,默认后缀为jpg
     */
    @Test
    void bufferedImageToByte() {
        BufferedImage bufferedImage = new BufferedImage(360,120,BufferedImage.TYPE_INT_RGB);
        byte[] bytes = ImgUtils.bufferedImageToByte(bufferedImage, "jpg");
        byte[] bytes2 = ImgUtils.bufferedImageToByte(bufferedImage);
    }




    /**
     * todo 图片缩放
     */
    @Test
    void resize() throws Exception {
        File file = new File("C:\\Users\\hanweb\\Desktop\\1.png");
        if (file.exists()) {
            byte[] bytes = ImgUtils.resize(file, 80, 80);
            ImgUtils.save(bytes,"C:\\Users\\hanweb\\Desktop","5","png");
//            byte[] resize = ImgUtils.resize(file, 100);
//            ImgUtils.saveImg(bytes,"C:\\Users\\hanweb\\Desktop\\4.jpg");
        }
//        ImgUtils.resize(new File())
    }

    /**
     * todo 获得图片宽度
     * @see ImgUtils#getWidth(String)
     */
    @Test
    void getWidth() {
        String path = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "slf4j.jpg";
        Integer width = null;
        try {
            width = ImgUtils.getWidth(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(width);
    }

    /**
     * todo 获得图片高度
     * @see ImgUtils#getHeight(String)
     */
    @Test
    void getHeight() {
        String path = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "slf4j.jpg";
        Integer height = null;
        try {
            height = ImgUtils.getHeight(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(height);
    }


}
