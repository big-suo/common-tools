package com.qs.commontools.utils.file;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author qiusuo
 * @since 2021-09-12
 */
@SpringBootTest
public class FileUtilsTest {
    String path = "/Users/qiusuo/Downloads";
    String filePath = "/Users/qiusuo/Desktop/成本预算.xlsx";


    /**
     * 获取File文件元素
     */
    @Test
    void fileTest() throws IOException {
        File file = new File(filePath);
        System.out.println(file.getAbsolutePath());
        System.out.println(file.getCanonicalPath());
        File file1 = new File("");
        System.out.println(file.exists());
        System.out.println(file1.exists());
    }

    /**
     * 获取文件大小
     */
    @Test
    void getSize() {
        try {
            String size = FileUtils.getSize(new File("/Users/qiusuo/Downloads/slf4j.jpg"));
            System.out.println(size);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 返回MultipartFile 类型
     * @throws IOException
     */
    @Test
    void getMultipartFile() throws IOException {
        MultipartFile multipartFile = FileUtils.getMultipartFile(filePath);
        MultipartFile multipartFile1 = FileUtils.getMultipartFile(new File(filePath));
        FileUtils.save(multipartFile,path,"测试保存","xlsx");
    }

    /**
     * 删除文件夹或文件
     */
    @Test
    void delete() throws Exception {
        FileUtils.delete("C:\\Users\\hanweb\\Desktop\\java\\com\\qs\\commontools");
    }
}
