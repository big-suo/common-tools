package com.qs.commontools.utils.file;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * excel工具类单元测试
 * @author qiusuo
 * @since 2021-11-14
 */
public class ExcelUtilsTest {

    /**
     * 对excel文件中写入数据
     */
    void write() {
        List<Map<String,String>> userList1 = new ArrayList<Map<String,String>>(); //第一页的数据userList1
        Map<String,String> map=new HashMap<String,String>();
        map.put("id", "111");
        map.put("name", "张三");
        map.put("password", "111！@#");

        Map<String,String> map2=new HashMap<String,String>();
        map2.put("id", "222");
        map2.put("name", "李四");
        map2.put("password", "222！@#");

        Map<String,String> map3=new HashMap<String,String>();
        map3.put("id", "33");
        map3.put("name", "王五");
        map3.put("password", "333！@#");
        userList1.add(map);
        userList1.add(map2);
        userList1.add(map3);
    }
}
