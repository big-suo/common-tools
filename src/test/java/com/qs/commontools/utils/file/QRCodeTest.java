package com.qs.commontools.utils.file;

import com.qs.commontools.utils.base.SystemUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

/**
 * 二维码测试类
 * @author qiusuo
 * @since 2021-11-15
 */
@SpringBootTest
public class QRCodeTest {

    /**
     * todo 绘制二维码
     * @see QRcodeTool#create()
     */
    @Test
    void buildQRcode() {
        QRcodeTool tool = QRcodeTool.getInstance();
        tool.setHyperlink("http://www.baidu.com");
        String logo = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "slf4j.jpg";
        tool.setLogo(logo);
        tool.setWidth(20);
        tool.setHeight(20);
        tool.setMargin(10);
        byte[] bytes;
        try {
            bytes = tool.create();
            String path = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                    "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                    "commontools" + File.separator + "common" + File.separator + "img" + File.separator ;
            ImgUtils.save(bytes,path,"baidu","jpg");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * todo 解析二维码图片
     * @see QRcodeTool#decode(File)
     * @see QRcodeTool#decode(String)
     */
    @Test
    void decodeQRcode() throws Exception {
        String path = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "baidu.jpg";
        String s = QRcodeTool.decode(path);
        System.out.println(s);
    }
}
