package com.qs.commontools.utils.file;

import com.qs.commontools.utils.base.SystemUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

/**
 * 水印工具类测试
 * @author qiusuo
 * @since 2021-11-15
 */
@SpringBootTest
public class WaterMarkToolTest {
    /**
     * todo 添加水印
     */
    @Test
    void waterMark() throws Exception {
        String src = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "slf4j.jpg";
        String icon = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator + "baidu.jpg";
        String water = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "img" + File.separator ;
        WaterMarkTool entity = WaterMarkTool.getInstance();
        entity.setText("文字水印");
        entity.setAngle(-45);
        entity.setHorizontalPosition(-100);
        entity.setVerticalPosition(-80);
        entity.setOpacity(1f);
        entity.setVerticalNum(10);
        entity.setHorizontalNum(10);
        entity.setHorizontalInterval(40);
        entity.setVerticalInterval(100);
        entity.setIconWidth(80);
        entity.setIconHeight(80);
//        entity.setIconPositionX(300);
//        entity.setIconPositionY(10);
//        entity.setIconOpacity(0.8f);
//        entity.setIconAngle(0);
        entity.setIcon(icon);
//        entity.setSrcImgPath("/Users/qiusuo/Desktop/1.jpeg");
//        entity.setTargerPath("/Users/qiusuo/Desktop/3.jpeg");
//        entity.setSrcImg("/Users/qiusuo/Desktop/2.jpeg");
        entity.setSrcImg(src);
//        entity.setTargerPath("/Users/qiusuo/Desktop/4.jpeg");
//        entity.setSrcImgPath("/Users/qiusuo/Desktop/6.jpg");
//        entity.setTargerPath("/Users/qiusuo/Desktop/7.jpg");
        byte[] bytes = entity.create();
        ImgUtils.save(bytes,water,"watermark","jpg");
    }
}
