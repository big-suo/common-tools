package com.qs.commontools.utils.file;

import com.qs.commontools.utils.base.SystemUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

/**
 * pdf工具单元测试
 * @author qiusuo
 * @since 2021-11-24
 */
@SpringBootTest
public class PdfToolTest {

    /**
     * pdf文件 转 base64字符串。
     * @see PdfTool#toBase64(File)
     */
    @Test
    void toBase64() {
        File file = new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "file" + File.separator + "pdfTest.pdf");
        try {
            String s = PdfTool.toBase64(file);
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void decodeBase64() {
        File file = new File(SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                "commontools" + File.separator + "common" + File.separator + "file" + File.separator + "pdfTest.pdf");
        try {
            String s = PdfTool.toBase64(file);
            byte[] bytes = PdfTool.decodeBase64(s);
            String path = SystemUtils.getProjectPath() + File.separator + "src" + File.separator +
                    "test" + File.separator + "java" + File.separator + "com" + File.separator + "qs" + File.separator +
                    "commontools" + File.separator + "common" + File.separator + "file"  ;
            ImgUtils.save(bytes,path,"pdfTest1","pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
