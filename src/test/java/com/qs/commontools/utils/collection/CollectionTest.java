package com.qs.commontools.utils.collection;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.stream.Stream;

/**
 * 集合单元测试
 * @author qiusuo
 * @since 2021-09-26
 */
@SpringBootTest
public class CollectionTest {

    /**
     * todo 拷贝List集合
     * {@link ListUtils#copyList(List)}}
     * {@link ListUtils#copyList(List, Integer)}
     * {@link ListUtils#copyList(Stream)}
     */
    @Test
    void copyList() {
        List a = Arrays.asList(new Integer[]{1,2,3});
        // 直接拷贝
        List b = ListUtils.copyList(a);
        // 跳过元素拷贝
        List c = ListUtils.copyList(a, 2);
        // 过滤拷贝
        List d = ListUtils.copyList(a.stream().filter(i -> (int) i <= 1));
        // 既过滤又跳过元素拷贝
        List e = ListUtils.copyList(a.stream().skip(2).filter(i -> (int) i <= 1));
    }


    /**
     * todo 遍历集合，将集合元素以特定符号拼接转为字符串
     * {@link ListUtils#foreach(List)}
     * {@link ListUtils#foreach(List, String, String, String)}
     */
    @Test
    void foreach() {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("5");
        list.add("4");
        list.add("1");
        // 默认分隔和前后缀
        System.out.println(ListUtils.foreach(list));
        // 自定义分隔和前后缀
        System.out.println(ListUtils.foreach(list,"(",null,")"));
    }


    /**
     * todo map 工具类的使用
     */
    @Test
    void mapTest() {
        Map map = new HashMap<>();
        map.put("boolean",false);
        map.put("boolean1",1);
        System.out.println(MapUtils.getBoolean(map, "boolean1"));
        System.out.println(MapUtils.getBoolean(map, "boolean1",true));
        System.out.println(MapUtils.getBooleanValue(map, "boolean1"));
        System.out.println(MapUtils.getBooleanValue(map, "boolean1",true));
//        MapUtils.getByte();
//        MapUtils.getByteValue()
//        MapUtils.getDouble();
//        MapUtils.getDoubleValue();
//        MapUtils.getFloat();
//        MapUtils.getFloatValue();
//        MapUtils.getInteger();
//        MapUtils.getIntValue();
//        MapUtils.getLong();
//        MapUtils.getLongValue();
//        MapUtils.getShort();
//        MapUtils.getShortValue();
//        MapUtils.getMap();
//        MapUtils.getObject();
//        MapUtils.getString();

    }


}
