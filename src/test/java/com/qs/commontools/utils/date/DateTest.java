package com.qs.commontools.utils.date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.Date;

/**
 * 日期工具类单元测试
 * @author qiusuo
 * @date 2021-09-29
 */
@SpringBootTest
public class DateTest {

    /**
     * todo DateUtils格式化日期
     * @see DateUtils#format(Date)
     * @see DateUtils#format(Date, String)
     */
    @Test
    void format() {
        System.out.println(DateUtils.format(new Date()));
        System.out.println(DateUtils.format(new Date(), "yyyy年MM月dd日"));
    }

    /**
     * todo DateUtils转换日期
     * @see DateUtils#parse(String)
     * @see DateUtils#parse(String, String)
     */
    @Test
    void parse() {
        try {
            System.out.println(DateUtils.parse("2021-09-29 10:25:01"));
            System.out.println(DateUtils.parse("2021-09-29 10:25", "yyyy-MM-dd HH:mm"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * todo DateUtils检验是否是闰年
     * @see DateUtils#checkLeapYear(String)
     * @see DateUtils#checkLeapYear(Integer)
     */
    @Test
    void checkLeapYear() {
        try {
            System.out.println(DateUtils.checkLeapYear("2008"));
            System.out.println(DateUtils.checkLeapYear(2018));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * todo 获取时间
     */
    @Test
    void getTime() throws ParseException {
        Date date = DateUtils.parse("2016-01-03 12:12:12");
        System.out.println("年：" + DateTool.getInstance(date).getYear());
        System.out.println("月：" + DateTool.getInstance(date).getMonth());
        System.out.println("日：" + DateTool.getInstance(date).getDay());
        System.out.println("时：" + DateTool.getInstance(date).getHour());
        System.out.println("分：" + DateTool.getInstance(date).getMinute());
        System.out.println("秒：" + DateTool.getInstance(date).getSecond());
        System.out.println("周：" + DateTool.getInstance(date).getWeek());
        System.out.println("一周中的序号：" + DateTool.getInstance(date).getDayOfWeek());
        System.out.println("一年中的序号：" + DateTool.getInstance(date).getDayOfYear());
        System.out.println("一年中的第几周(中国算法)：" + DateTool.getInstance(date).getWeekOfYear());
        System.out.println("一年中的第几周(国外算法)：" + DateTool.getInstance(date).getWeekOfYear(false));
        System.out.println("该月的天数：" + DateTool.getInstance(date).getDaysOfMonth());
    }


    /**
     * todo DateTool方法测试
     * @see DateTool#setDate(Date)
     * @see DateTool#setFormat(String)
     */
    @Test
    void dateTool() throws ParseException {
        Date date = DateUtils.parse("2008-01-12 00:00:00");
        DateTool tool = DateTool.getInstance();
        // 设置日期，不设置就是默认当前时间
        tool.setDate(date);
        // 设置格式化字符串
        tool.setFormat("yyyy-MM-dd");
        // 获取格式化后的字符串,这里未对日期进行设置处理所以获取到的就是原设置的日期
        System.out.println(tool.getFormat()); // 2008-01-12
        // 检查未操作时间前的日期是否是闰年
        System.out.println(tool.checkLeapYear());  // true
        // 设置年份 -1
        tool.setYear(-1);
        // 设置月份 +1
        tool.setMonth(1);
        // 设置日期 +1
        tool.setDay(1);
        // 设置小时 +12
        tool.setHour(12);
        // 设置分钟 +12
        tool.setMinute(12);
        // 设置秒数 +12
        tool.setSecond(12);
        // 获取格式化后的字符串,这里已经对日期进行了设置，获取到的就是处理后的日期
        System.out.println(tool.getFormat("yyyy-MM-dd HH:mm")); // 2007-02-13 12:12
        // 获取处理过的 年
        System.out.println(tool.getYear()); // 2007
        // 获取处理过的 月
        System.out.println(tool.getMonth()); // 2
        // 获取处理过的 日
        System.out.println(tool.getDay());  // 13
        // 获取处理过的 时
        System.out.println(tool.getHour());  // 12
        // 获取处理过的 分
        System.out.println(tool.getMinute()); // 12
        // 获取处理过的 秒
        System.out.println(tool.getSecond());  // 12
        // 获取处理过后的时间位于周几
        System.out.println(tool.getWeek());  // 星期二
        // 检查操作后的时间日期是一年中的第几周，以中国算法计算，周一为一周的第一天
        System.out.println(tool.getWeekOfYear());  // 7
        // 检查操作后的时间日期是一年中的第几周，以国外算法计算，周末为一周的第一天
        System.out.println(tool.getWeekOfYear(false));  // 7
        // 检查操作后的时间日期是一周中的第几天
        System.out.println(tool.getDayOfWeek());  // 3
        // 检查操作后的时间日期是一年中的第几天
        System.out.println(tool.getDayOfYear());  // 44
        // 检查操作后的时间日期所在的月份共有几天
        System.out.println(tool.getDaysOfMonth());  // 28
        // 检查操作后的时间日期是否是闰年
        System.out.println(tool.checkLeapYear());  // false
        // 获取被操作后的日期
        Date date1 = tool.getDate(); // Tue Feb 13 12:12:12 CST 2007
        System.out.println(date1);
    }
}
