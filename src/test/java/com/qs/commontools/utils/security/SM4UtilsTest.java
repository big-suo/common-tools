package com.qs.commontools.utils.security;

import com.qs.commontools.common.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * SM4加密单元测试
 * @author qiusuo
 * @since 2021-11-28
 */
@SpringBootTest
public class SM4UtilsTest {

    /**
     * 创建密钥
     * @see SM4Utils#createKey()
     * @see SM4Utils#createKey(int)
     */
    @Test
    void createKey() {
        try {
            // 32位16进制密钥
            System.out.println(SM4Utils.createKey()); // 471599ae03f926fab4f5dc14c77f9682
            // 64位16进制密钥
            System.out.println(SM4Utils.createKey(256)); // bed455c0cdfd5138291589a35ead68c49d8a442ae6a5c24002d8eadc24f52511
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * ECB加密
     * @see SM4Utils#encryptEcb(String, String)
     */
    @Test
    void encryptEcb () {
        String s = "{\"age\":43,\"id\":\"6043867afe594bd48cfd09847a020a56\",\"pwd\":\"admin@123\",\"username\":\"张三\"}";
        try {
            String s1 = SM4Utils.encryptEcb("471599ae03f926fab4f5dc14c77f9682", s);
            // 7871873bfde9a987566de6dc525ccff97869d606a0e22c84335168144c1a5e2dc08c204588596a281225ac0733f79b107cfd58ed5db466d7a81bac877772a041da07602fb5927f1d666be8fac1a9b5a52cc8d8f4ce75300acddaa6cb270da31c
            System.out.println(s1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 解密
     * @see SM4Utils#decryptEcb(String, String)
     */
    @Test
    void decryptEcb() {
        String s = null;
        try {
            s = SM4Utils.decryptEcb("471599ae03f926fab4f5dc14c77f9682", "7871873bfde9a987566de6dc525ccff97869d606a0e22c84335168144c1a5e2dc08c204588596a281225ac0733f79b107cfd58ed5db466d7a81bac877772a041da07602fb5927f1d666be8fac1a9b5a52cc8d8f4ce75300acddaa6cb270da31c");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(s);
    }

    /**
     * 校验
     * @see SM4Utils#verifyEcb(String, String, String)
     */
    @Test
    void verifyEcb() {
        String s = "{\"age\":43,\"id\":\"6043867afe594bd48cfd09847a020a56\",\"pwd\":\"admin@123\",\"username\":\"张三\"}";
        try {
            System.out.println(SM4Utils.verifyEcb("471599ae03f926fab4f5dc14c77f9682","7871873bfde9a987566de6dc525ccff97869d606a0e22c84335168144c1a5e2dc08c204588596a281225ac0733f79b107cfd58ed5db466d7a81bac877772a041da07602fb5927f1d666be8fac1a9b5a52cc8d8f4ce75300acddaa6cb270da31c",s));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
