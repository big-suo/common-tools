package com.qs.commontools.utils.security;

import com.qs.commontools.utils.base.SystemUtils;
import org.bouncycastle.math.ec.ECPoint;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.math.BigInteger;

/**
 * 编码和解码的单元测试
 * @author qiusuo
 * @since 2021-11-10
 * @date 2021-11-10
 */
@SpringBootTest
public class SM2UtilsTest {

//    /**
//     * 获得公钥私钥
//     * @see SM2Utils#getPublicKey()
//     * @see SM2Utils#getPrivateKey()
//     */
//    @Test
//    void getKey(){
//        SM2Utils sm2Utils = SM2Utils.getInstance();
//        System.out.println(sm2Utils.getPublicKey());
//        System.out.println(sm2Utils.getPrivateKey());
//    }
//
//    /**
//     * 加密
//     */
//    @Test
//    void encrypt() throws IOException {
//        String a = "圣诞节和干撒是非观数据上打防结合上的解放后4返回ddfsgasfdas";
//        SM2Utils sm2Utils = SM2Utils.getInstance();
//        String encrypt = sm2Utils.encrypt(TransformUtils.hexStrToByte(sm2Utils.getPrivateKey()), a.getBytes());
//        System.out.println(encrypt);
//    }
//
//    /**
//     * 解密
//     */
//    @Test
//    void decrypt(){
//        SM2Utils sm2Utils = SM2Utils.getInstance();
//    }

    /**
     * 创建私钥
     * @see SM2Utils#createPrivateKey()
     */
    @Test
    void createPrivateKey() {
        BigInteger privateKey = SM2Utils.createPrivateKey();
    }

    /**
     * 获得私钥字符串
     * @see SM2Utils#toStrPrivateKey(BigInteger)
     */
    @Test
    void toStrPrivateKey() {
        BigInteger privateKey = SM2Utils.createPrivateKey();
        String s = SM2Utils.toStrPrivateKey(privateKey);
        System.out.println(s);
    }

    /**
     * 创建公钥
     * @see SM2Utils#createPublicKey(BigInteger) )
     */
    @Test
    void createPublicKey() {
        ECPoint publicKey = SM2Utils.createPublicKey(SM2Utils.createPrivateKey());
    }

    /**
     * 获得公钥字符串
     * @see SM2Utils#toStrPublicKey(ECPoint)
     */
    @Test
    void toStrPublicKey() {
        ECPoint publicKey = SM2Utils.createPublicKey(SM2Utils.createPrivateKey());
        String s = SM2Utils.toStrPublicKey(publicKey);
        System.out.println(s);
    }

    /**
     * 字符串私钥转BigInteger私钥
     * @see SM2Utils#toBigIntegerPrivateKey(String)
     */
    @Test
    void toBigIntegerPrivateKey() {
        // 创建一个新的私钥
        BigInteger privateKey = SM2Utils.createPrivateKey();
        // 私钥转字符串私钥
        String s = SM2Utils.toStrPrivateKey(privateKey);
        // 再将私钥字符串转为BigInteger类型
        BigInteger bigInteger = SM2Utils.toBigIntegerPrivateKey(s);
        // 比较两数，相等返回0，小于返回-1，大于返回1
        System.out.println(privateKey.compareTo(bigInteger));
    }

    /**
     * 导出公钥私钥
     * @see SM2Utils#exportPrivateKey(BigInteger, String)
     * @see SM2Utils#exportPublicKey(ECPoint, String)
     */
    @Test
    void export() {
        String L = File.separator;

        String privatePath = SystemUtils.getProjectPath() + L + "src" + L +
                "test" + L+ "java" + L + "com" + L + "qs" + L +
                "commontools" + L + "common" + L + "file" + L + "sm2" + L + "privateKey.pem";
        String publicPath = SystemUtils.getProjectPath() + L + "src" + L +
                "test" + L+ "java" + L + "com" + L + "qs" + L +
                "commontools" + L + "common" + L + "file" + L + "sm2" + L + "publicKey.pem";

        // 私钥
        BigInteger privateKey = SM2Utils.createPrivateKey();
        System.out.println();
        // 公钥
        ECPoint publicKey = SM2Utils.createPublicKey(privateKey);

        // 149F59C0EB5618BF203D099CC834CBE07B5FD339CE87B56A5E1381D621405F02
        // 04B93B5F01ECC5AE7C1A556E0232F93FABB04D6C91F9CE3D1731D3F62A7269C435DA002E6D59730DB96AFC023A62BD1677DF731695554B95643CDDC61FEEAD0C1C
        System.out.println(SM2Utils.toStrPrivateKey(privateKey));
        System.out.println(SM2Utils.toStrPublicKey(publicKey));

        //导出
        SM2Utils.exportPrivateKey(privateKey, privatePath);
        SM2Utils.exportPublicKey(publicKey, publicPath);

    }

    /**
     * 导入公私密钥
     */
    @Test
    void import1() {
        String L = File.separator;
        String privatePath = SystemUtils.getProjectPath() + L + "src" + L +
                "test" + L+ "java" + L + "com" + L + "qs" + L +
                "commontools" + L + "common" + L + "file" + L + "sm2" + L + "privateKey.pem";
        String publicPath = SystemUtils.getProjectPath() + L + "src" + L +
                "test" + L+ "java" + L + "com" + L + "qs" + L +
                "commontools" + L + "common" + L + "file" + L + "sm2" + L + "publicKey.pem";
        BigInteger bigInteger = SM2Utils.importPrivateKey(privatePath);
        ECPoint ecPoint = SM2Utils.importPublicKey(publicPath);

        // 149F59C0EB5618BF203D099CC834CBE07B5FD339CE87B56A5E1381D621405F02
        // 04B93B5F01ECC5AE7C1A556E0232F93FABB04D6C91F9CE3D1731D3F62A7269C435DA002E6D59730DB96AFC023A62BD1677DF731695554B95643CDDC61FEEAD0C1C
        System.out.println(SM2Utils.toStrPrivateKey(bigInteger));
        System.out.println(SM2Utils.toStrPublicKey(ecPoint));
    }


}
