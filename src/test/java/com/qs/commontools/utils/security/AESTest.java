package com.qs.commontools.utils.security;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 编码和解码的单元测试
 * @author qiusuo
 * @since 2021-09-22
 */
@SpringBootTest
public class AESTest {

    /**
     * todo AES创建密钥或盐度值
     * @see AESUtils#createKey()
     */
    @Test
    void AES() throws Exception {
        System.out.println(AESUtils.createKey());
    }


    /**
     * todo ECB模式加解密
     * @see AESUtils#decrypt(String, String)
     * @see AESUtils#encrypt(String, String)
     */
    @Test
    void ecb() {
        String createAuthKey = "W7HZY7FXZH7LAEHQ";
        // 加密
        String ecbm = AESUtils.encrypt("2021-10-14",createAuthKey);
        // 解密
        String ecbj = AESUtils.decrypt(ecbm,createAuthKey);
        System.out.println(ecbm);
        System.out.println(ecbj);
    }

    /**
     * todo CBC模式加解密
     * @see AESUtils#decrypt(String, String, String)
     * @see AESUtils#encrypt(String, String, String)
     */
    @Test
    void cbc() {
        String createAuthKey = "W7HZY7FXZH7LAEHQ";
        String iv = "E2KNHA27N5ODS5O7";
        // 加密
        String cbcm = AESUtils.encrypt("2021-10-14",createAuthKey,iv);
        // 解密
        String cbcj = AESUtils.decrypt(cbcm,createAuthKey,iv);
        System.out.println(cbcm);
        System.out.println(cbcj);
    }

}
