package com.qs.commontools.utils.security;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 安全方法
 * @author qiusuo
 * @since 2021-10-31
 */
@SpringBootTest
public class SecurityUtilsTest {

    /**
     * todo 脱敏名称
     * @see SecurityUtils#desensitizedName(String) 
     */
    @Test
    void desensitizedName() {
        System.out.println(SecurityUtils.desensitizedName("张三丰"));
    }

    /**
     * todo 脱敏手机号码
     * @see SecurityUtils#desensitizedPhoneNumber(String, Integer, Integer)
     * @see SecurityUtils#desensitizedPhoneNumber(String)
     */
    @Test
    void desensitizedPhoneNumber() {
        System.out.println(SecurityUtils.desensitizedPhoneNumber("13814602018",2,4));
        System.out.println(SecurityUtils.desensitizedPhoneNumber("13814602018"));
    }

    /**
     * todo 脱敏身份证号码
     * @see SecurityUtils#desensitizedIdNumber(String)
     * @see SecurityUtils#desensitizedIdNumber(String, Integer, Integer) 
     */
    @Test
    void desensitizedIdNumber() {
        System.out.println(SecurityUtils.desensitizedIdNumber("320611199609141543"));
        System.out.println(SecurityUtils.desensitizedIdNumber("320611199609141543",3,4));
    }

    /**
     * todo 脱敏地址
     * @see SecurityUtils#desensitizedAddress(String) 
     */
    @Test
    void desensitizedAddress() {
        System.out.println(SecurityUtils.desensitizedAddress("为i符合我飞机符合让肌肤黑人u入会费俄UI日发货俄如何"));
        System.out.println(SecurityUtils.desensitizedAddress("为的"));
        System.out.println(SecurityUtils.desensitizedAddress("为的当然烦人烦人"));
    }
}
