package com.qs.commontools.utils.security;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author qiusuo
 * @since
 */
@SpringBootTest
public class MD5UtilsTest {

    /**
     * 加密
     * @see MD5Utils#encrypt(String)
     */
    @Test
    void encode() {
        System.out.println(MD5Utils.encrypt("123456"));
    } //e10adc3949ba59abbe56e057f20f883e

    /**
     * 校验密码
     *
     */
    @Test
    void check() {
        System.out.println(MD5Utils.encrypt("123456").equals("e10adc3949ba59abbe56e057f20f883e"));
    }
}
