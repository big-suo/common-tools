package com.qs.commontools.utils.base;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

/**
 * 字符串工具类测试
 * @author qiusuo
 * @since 2021-09-18
 */
@SpringBootTest
public class StringTest {
    // todo uuid测试
    /**
     * 获得简单的uuid
     * @see StringUtils#getSimpleUUID()
     */
    @Test
    void getSimpleUUID() {
        System.out.println(StringUtils.getSimpleUUID());
    }

    /**
     * 获得正常的uuid
     * @see StringUtils#getUUID()
     */
    @Test
    void getUUID() {
        System.out.println(StringUtils.getUUID());
    }

    // todo 驼峰和下划线互转测试
    /**
     * 驼峰转下划线
     */
    @Test
    void lowerCamelCaseToLine() {
        System.out.println(StringUtils.lowerCamelCaseToLine("UserName"));
    }

    /**
     * 下划线转驼峰
     */
    @Test
    void lineToLowerCamelCase() {
        System.out.println(StringUtils.lineToLowerCamelCase("_" +
                "user_name"));
    }

    /**
     * todo 字符串存在性的判断
     */
    @Test
    void strExist() {
        System.out.println(StringUtils.isBlank(null));
        System.out.println(StringUtils.isBlank("") );
        System.out.println( StringUtils.isBlank("  "));
        System.out.println( StringUtils.isBlank("        ") );
        System.out.println(StringUtils.isBlank("\t \n \f \r"));
        System.out.println(StringUtils.isBlank("fff") );
        System.out.println(StringUtils.isBlank("ffff "));
        System.out.println(StringUtils.isEmpty(null));
        System.out.println(StringUtils.isEmpty(""));
        System.out.println(StringUtils.isEmpty(" "));
        System.out.println(StringUtils.isEmpty("   "));
        System.out.println(StringUtils.isEmpty("fff"));
        System.out.println(StringUtils.isEmpty(" fff ") );
    }

    /**
     * todo 字符串等值判断
     */
    @Test
    void equals() {
        System.out.println(StringUtils.equals(""," "));
        System.out.println(StringUtils.equals(null," "));
        System.out.println(StringUtils.equals("12","12"));
        System.out.println(StringUtils.equalsByTrim("22 " ,"22 "));
        System.out.println(StringUtils.equalsIgnoreCase("Udd", "udd"));
        System.out.println(StringUtils.equalsByTrimIgnoreCase("Udd", "udd "));
    }

    /**
     * todo 验证码
     */
    @Test
    void verification() {
        System.out.println(StringUtils.getVerification());
        System.out.println(StringUtils.getVerification(6));
        System.out.println(StringUtils.getVerification(new char[]{'1','2','A','a','B'}));
        System.out.println(StringUtils.getVerification(3 , new char[]{'1','2','A','a','B'}));
    }

    /**
     * todo 左侧截取
     */
    @Test
    void left() {
        System.out.println(StringUtils.left("0123456789",3));
    }

    /**
     * todo 右侧截取
     */
    @Test
    void right() {
        System.out.println(StringUtils.right("0123456789",4));

    }

    /**
     * todo 如果字符串str是以remove开始，则去掉这个开始，然后返回，否则返回原来的串
     */
    @Test
    void removeStart() {
        System.out.println(StringUtils.removeStart("www.baidu.com","wwww"));
        System.out.println(StringUtils.removeStart("www.baidu.com","www"));
    }


    /**
     * todo 字符串左侧补全
     */
    @Test
    void leftPad() {
        System.out.println(StringUtils.leftPad("张",2));
        System.out.println(StringUtils.leftPad("张",6,"*#"));
        System.out.println(StringUtils.leftPad("张",5,'f'));
    }

    /**
     * todo 字符串右侧补全
     */
    @Test
    void rightPad() {
        System.out.println(StringUtils.rightPad("张",2));
        System.out.println(StringUtils.rightPad("张",6,"*#"));
        System.out.println(StringUtils.rightPad("张",5,'f'));
    }

    @Test
    void tr() {
        System.out.println(StringUtils.rightPad(StringUtils.left("370100000000",11),12,"1"));
        System.out.println("370100000000");
        String[] strings = new String[]{"370000","370100","370200","370300","370400","370500","370600",
                "370700","370800","370900","371000","371100","371200","371300","371400","371500","371600","371700"};
        System.out.println(Arrays.asList(strings).contains("370101"));
        System.out.println(Arrays.asList(strings).contains("370100"));
    }

}
