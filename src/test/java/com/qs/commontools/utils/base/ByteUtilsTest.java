package com.qs.commontools.utils.base;

import org.junit.jupiter.api.Test;

/**
 * 字节工具类单元测试。
 * @author qiusuo
 * @since 2021-11-29
 * @see ByteUtils
 */
public class ByteUtilsTest {

    /**
     * 两字节数组比较是否相等
     * @see ByteUtils#isEqual(byte[], byte[])
     */
    @Test
    void isEqual() {
        byte[] a = {0x01, 0x01, 0x02, 0x34, 0x71, 0x2b, 0x5f, 0x30};
        byte[] b = {0x01, 0x01, 0x02, 0x04, 0x71, 0x2b, 0x5f, 0x30};
        byte[] c = {0x01, 0x01, 0x02, 0x34, 0x71, 0x2b, 0x5f, 0x30};
        System.out.println(ByteUtils.isEqual(a,b)); // false
        System.out.println(ByteUtils.isEqual(a,c)); // true

        String d = "219DE16F68A55E1220A67324B213F2B42C2D0024E3AD0A70AC634A994908D1A1";
        String e = "219de16f68a55e1220a67324b213f2b42c2d0024e3ad0a70ac634a994908d1a1";
        System.out.println(ByteUtils.isEqual(TransformUtils.hexStrToByte(d),TransformUtils.hexStrToByte(e))); //
    }


}
