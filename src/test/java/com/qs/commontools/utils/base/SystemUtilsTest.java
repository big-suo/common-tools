package com.qs.commontools.utils.base;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

/**
 * 系统工具单元测试类。
 *
 * @author qiusuo
 * @since 2021-11-25
 * @see SystemUtils
 */
@SpringBootTest
public class SystemUtilsTest {

    /**
     * 获得当前项目的路径。
     * @see SystemUtils#getProjectPath()
     */
    @Test
    void getProjectPath() {
        System.out.println(SystemUtils.getProjectPath());
    }


    /**
     * 获得用户的主目录。
     * @see SystemUtils#getHome()
     */
    @Test
    void getHome() {
        System.out.println(SystemUtils.getHome());
    }

    /**
     * 获取类所在的路径。
     * @see SystemUtils#getCurrentPath(Object)
     */
    @Test
    void getCurrentPath() {
        System.out.println(SystemUtils.getCurrentPath(this));
    }

    /**
     * 获得系统账号名称
     * @see SystemUtils#getUser()
     */
    @Test
    void getUser() {
        System.out.println(SystemUtils.getUser());
    }

    /**
     * 获得操作系统名称
     * @see SystemUtils#getOsName()
     */
    @Test
    void getOsName() {
        System.out.println(SystemUtils.getOsName()); // Windows 10
    }

    /**
     * 获得操作系统架构
     * @see SystemUtils#getOsArch()
     */
    @Test
    void getOsArch() {
        System.out.println(SystemUtils.getOsArch()); // amd64
    }

    /**
     * 获得操作系统版本
     * @see SystemUtils#getOsVersion()
     */
    @Test
    void getOsVersion() {
        System.out.println(SystemUtils.getOsVersion()); // 10.0
    }

    /**
     * 获得文件分隔符
     * @see  SystemUtils#getSeparatorFile()
     */
    @Test
    void getSeparatorFile() {
        System.out.println(SystemUtils.getSeparatorFile());
        System.out.println(File.separator);
    }

    /**
     * 获得路径分隔符
     * @see  SystemUtils#getSeparatorPath()
     */
    @Test
    void getSeparatorPath() {
        System.out.println(SystemUtils.getSeparatorPath());
    }

    /**
     * 获得行分割符
     * @see  SystemUtils#getSeparatorLine()
     */
    @Test
    void getSeparatorLine() {
        System.out.println(SystemUtils.getSeparatorLine());
    }
}
