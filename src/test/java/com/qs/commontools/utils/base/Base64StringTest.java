package com.qs.commontools.utils.base;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * base64编码和String工具类单元测试。
 * @author qiusuo
 * @since 2021-11-24
 */
@SpringBootTest
public class Base64StringTest {
    /**
     * base64相关测试
     */
    @Test
    void base64() {
//        System.out.println(StringUtils.strConvertBase64("I LOVE YOU 我爱你"));
//        System.out.println(StringUtils.base64ConvertStr("SSBMT1ZFIFlPVSDmiJHniLHkvaA="));
//        System.out.println(StringUtils.base64ConvertStr("SSBMT1ZFIFlPVSDmiJHniLHkvaA=","GBK"));
//        try {
//            System.out.println(StringUtils.base64StrToByte("SSBMT1ZFIFlPVSDmiJHniLHkvaA=").toString());
//            System.out.println(StringUtils.base64ByteToStr(StringUtils.base64StrToByte("SSBMT1ZFIFlPVSDmiJHniLHkvaA=")));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
