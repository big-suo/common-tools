package com.qs.commontools.utils.base;

import com.qs.commontools.utils.security.SM4Utils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 转换工具类单元测试
 * @author qiusuo
 * @since 2021-11-29
 */
@SpringBootTest
public class TransformUtilsTest {

    /**
     * @see com.qs.commontools.utils.base.TransformUtils#hexByteToStr(byte[])
     */
    @Test
    void hexByteToStr() throws Exception {
        String key = SM4Utils.createKey();
        byte[] bytes = TransformUtils.hexStrToByte(key);
        System.out.println(TransformUtils.hexByteToStr(bytes));

    }

    /**
     * @see TransformUtils#hexStrToByte(String)
     */
    @Test
    void hexStrToByte() throws Exception {
        String key = SM4Utils.createKey();
        System.out.println(TransformUtils.hexStrToByte(key));
    }
}
