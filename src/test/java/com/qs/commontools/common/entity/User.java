package com.qs.commontools.common.entity;


import com.qs.commontools.utils.common.CommonEnity;

import java.io.File;
import java.util.List;

/**
 * @author qiusuo
 * @since 2021-09-12
 */
public class User extends CommonEnity implements Comparable{
    private String username;
    private File file;
    private File file1;
    private String pwd;
    private Boolean isboy;
    private Integer age;
    private List<User> friends;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getFile1() {
        return file1;
    }

    public void setFile1(File file1) {
        this.file1 = file1;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Boolean getIsboy() {
        return isboy;
    }

    public void setIsboy(Boolean isboy) {
        this.isboy = isboy;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
