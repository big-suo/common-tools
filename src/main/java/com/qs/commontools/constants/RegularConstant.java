package com.qs.commontools.constants;

import java.util.regex.Pattern;

/**
 * 正则常量类
 * @author qiusuo
 * @since 2021-09-29
 */
public interface RegularConstant {
    /**
     * 是否是年份范围0001-9999。
     */
    Pattern IS_YEAR = Pattern.compile("^[0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}&");

    /**
     * 香港手机号码8位数，5|6|8|9开头+7位任意数。
     */
    Pattern HK_PATTERN = Pattern.compile("^(5|6|8|9)\\d{7}$");

    /**
     * 大陆手机号码11位数。
     * 匹配格式：前三位固定格式+后8位任意数，
     * 此方法中前三位格式有：
     * <ul>
     *    <li>13+任意数</li>
     *    <li>145,147,149</li>
     *    <li>15+除4的任意数(不要写^4，这样的话字母也会被认为是正确的)</li>
     *    <li>166</li>
     *    <li>17+3,5,6,7,8</li>
     *    <li>18+任意数</li>
     *    <li>198,199</li>
     * </ul>
     */
    Pattern CHINA_PHONE = Pattern.compile("^((13[0-9])|(14[0,1,4-9])|(15[0-3,5-9])|(16[2,5,6,7])|(17[0-8])|(18[0-9])|(19[0-3,5-9]))\\d{8}$");

    /**
     * 大陆座机正则。
     */
    Pattern CHINA_LANDLINE = Pattern.compile("^(0\\d{2,3})-?(\\d{7,8})$");

    /**
     * 是否是正整数。
     */
    Pattern POSITIVE_INTEGER = Pattern.compile("^[1-9]+[0-9]*$");

    /**
     * 非负整数。
     */
    Pattern POSITIVE_ZERO_INTEGER = Pattern.compile("^[0-9]+[0-9]*$");

    /**
     * 非正整数。
     */
    Pattern NEGATIVE_ZERO_INTEGER = Pattern.compile("^-[1-9]\\d*|0$");
//    Pattern NEGATIVE_ZERO_INTEGER = Pattern.compile("^((-\\d+)|(0+))$");

    /**
     * 是否是整数。
     */
    Pattern  INTEGER = Pattern.compile("^(0|[1-9][0-9]*|-[1-9][0-9]*)$");

    /**
     * 是否是数字。
     */
    Pattern NUM = Pattern.compile("^-?\\d+(\\.\\d+)?$");

    /**
     * 是否是浮点数
     */
    Pattern NUM_FLOAT = Pattern.compile("^-?([1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*|0?\\.0+|0)$");
//    Pattern NUM_FLOAT = Pattern.compile("^(-?\d+)(\.\d+)?$");

    /**
     * 非负浮点数
     */
    Pattern NUM_FLOAT_POSITIVE_ZERO = Pattern.compile("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*|0?\\.0+|0$");
//    Pattern NUM_FLOAT_POSITIVE_ZERO = Pattern.compile("^\\d+(\\.\\d+)?$");

    /**
     * 正浮点数
     */
//    Pattern NUM_FLOAT_POSITIVE = Pattern.compile("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$"); //包含正整数
    Pattern NUM_FLOAT_POSITIVE = Pattern.compile("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*$");

    /**
     * 非正浮点数
     */
    Pattern NUM_FLOAT_NEGATIVE_ZERO = Pattern.compile("^(-([1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*))|0?\\.0+|0$");
//    Pattern NUM_FLOAT_NEGATIVE_ZERO = Pattern.compile("^((-\\d+(\\.\\d+)?)|(0+(\\.0+)?))$");

    /**
     * 负浮点数
     */
    Pattern NUM_FLOAT_NEGATIVE = Pattern.compile("^-([1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*)$");
//    Pattern NUM_FLOAT_NEGATIVE = Pattern.compile("^(-(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*)))$");

    /**
     * 电子邮箱。
     */
    Pattern EMAIL = Pattern.compile("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");

    /**
     * 邮政编码。
     */
    Pattern POSTAL_CODE = Pattern.compile("^[1-9]\\d{5}$");

    /**
     * 账号1。
     */
    Pattern ACCOUNT1 = Pattern.compile("^[a-zA-Z][a-zA-Z0-9_]{4,15}$");

    /**
     * 账号2。
     */
    Pattern ACCOUNT2 = Pattern.compile("^[a-zA-Z\\d\\(\\)()\\-\\_\\.\\s$¥]+$");

    /**
     * 姓名。
     */
    Pattern ACCOUNT_NAME = Pattern.compile("^[\\\\u4e00-\\\\u9fa5a-zA-Z]+$");

    /**
     * 普通身份证号码。
     */
    Pattern ID_CARD = Pattern.compile("(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)");

    /**
     * 18位身份证号码精确校验。
     */
    Pattern ID_CARD18 = Pattern.compile("^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{4}$");

    /**
     * 15位身份证号码精确校验。
     */
    Pattern ID_CARD15 = Pattern.compile("^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$");

    /**
     * 身份证后六位校验。
     */
    Pattern ID_CARD_SUFFIX6 = Pattern.compile("^(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$");

    /**
     * 汉字。
     */
    Pattern ZH_CN = Pattern.compile("^[\\u4e00-\\u9fa5]{0,}$");

    /**
     * 26个英文字母组成的字符串。
     */
    Pattern EN_STR = Pattern.compile("^[A-Za-z]+$");

    /**
     * 由26个大写英文字母组成的字符串。
     */
    Pattern EN_UP = Pattern.compile("^[A-Z]+$");

    /**
     * 由26个小写英文字母组成的字符串。
     */
    Pattern EN_LOW = Pattern.compile("^[a-z]+$");

    /**
     * 由数字和26个英文字母组成的字符串.
     */
    Pattern EN_NUM = Pattern.compile("^[A-Za-z0-9]+$");

    /**
     * 中文、英文、数字包括下划线
     */
    Pattern ZH_EN_NUM_LAIN = Pattern.compile("^[\\u4E00-\\u9FA5A-Za-z0-9_]+$");

    /**
     * IP地址
     */
    Pattern IP = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

    /**
     * 标签对
     */
    Pattern TAG = Pattern.compile("^<([a-z]+)([^<]+)*(?:>(.*)<\\/\\1>|\\s+\\/>)$");

    /**
     * Http类型URL
     */
    Pattern HTTP_URL = Pattern.compile("^(http|https):\\/\\/([\\w.]+\\/?)\\S*$");

    /**
     * 带下划线
     */
    Pattern LINEPATTERN = Pattern.compile("_(\\w)");

    /**
     * 大写字母
     */
    Pattern HUMPPATTERN = Pattern.compile("[A-Z]");


}
