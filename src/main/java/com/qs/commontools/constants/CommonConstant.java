package com.qs.commontools.constants;


/**
 * CommonTool常用实体类
 * @author qiusuo
 * @since  2021-11-10
 */
public interface CommonConstant {

    /**
     * 常用字符串：json
     */
    String STR_JSON = "json";
    /**
     * 常用字符串
     */
    String STR_EMPTY = "";




    /**
     * 常量数字：0
     */
    Integer NUM_INTEGER_ZORE = 0;
    /**
     * 常量字符串数字："0"
     */
    String NUM_STRING_ZORE = "0";
    /**
     * 常量字符串数字："1"
     */
    String NUM_STRING_ONE = "1";
    /**
     * 常量数字：10
     */
    Integer NUM_INTEGER_TEN = 10;
    /**
     * 常量数字：16
     */
    Integer NUM_INTEGER_SIXTEEN = 16;
    /**
     * 数字常量：20
     */
    Integer NUM_INTEGER_TWENTY  = 20;
    /**
     * 数字常量：65
     */
    Integer NUM_INTEGER_SIXTY_FIVE = 65;
    /**
     * 常量数字：128
     */
    Integer NUM_INTEGER_ONE_HUNDRED_AND_TWENTY_EIGHT = 128;
    /**
     * 常量数字：256
     */
    Integer NUM_INTEGER_TWO_HUNDRED_AND_FIFTY_SIX = 256;
    /**
     * 常量数字：1G
     */
    Integer NUM_INTEGER_GB = 1024 * 1024 * 1024;
    /**
     * 浮点数字：1G
     */
    Double NUM_DOUBLE_GB = 1024.0 * 1024.0 * 1024.0;
    /**
     * 常量数字：1M
     */
    Integer NUM_INTEGER_MB = 1024 * 1024;
    /**
     * 浮点数字：1M
     */
    Double NUM_DOUBLE_MB = 1024.0 * 1024.0;
    /**
     * 常量数字：1K
     */
    Integer NUM_INTEGER_KB = 1024;
    /**
     * 浮点数字：1K
     */
    Double NUM_DOUBLE_KB = 1024.0;



    /**
     * 编码：UTF-8
     */
    String CODE_FORMAT_UTF_8 = "UTF-8";
    /**
     * 编码：UTF8
     */
    String CODE_FORMAT_UTF8 = "UTF8";
    /**
     * 编码：GBK
     */
    String CODE_FORMAT_GBK = "GBK";
    /**
     * 编码：Unicode
     */
    String CODE_FORMAT_UNICODE = "Unicode";
    /**
     * 编码:ISO8859-1
     */
    String CODE_FORMAT_ISO88591 = "ISO8859-1";
}
