package com.qs.commontools.constants;

import java.math.BigInteger;

/**
 * 解码和编码的常量类
 * @author qiusuo
 * @since 2021-09-22
 */
public interface SecurityConstant extends CommonConstant {
    /**
     * 基点G, G=(xg,yg),其介记为n
     */
    BigInteger SM2_N = new BigInteger(
            "FFFFFFFE" + "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF" + "7203DF6B" + "21C6052B" + "53BBF409" + "39D54123", 16);
    /**
     * 素数
     */
    BigInteger SM2_P = new BigInteger(
            "FFFFFFFE" + "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF" + "00000000" + "FFFFFFFF" + "FFFFFFFF", 16);
    /**
     * 系数a
     */
    BigInteger SM2_A = new BigInteger(
            "FFFFFFFE" + "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF" + "00000000" + "FFFFFFFF" + "FFFFFFFC", 16);
    /**
     * 系数b
     */
    BigInteger SM2_B = new BigInteger(
            "28E9FA9E" + "9D9F5E34" + "4D5A9E4B" + "CF6509A7" + "F39789F5" + "15AB8F92" + "DDBCBD41" + "4D940E93", 16);
    /**
     * 坐标x
     */
    BigInteger SM2_GX = new BigInteger(
            "32C4AE2C" + "1F198119" + "5F990446" + "6A39C994" + "8FE30BBF" + "F2660BE1" + "715A4589" + "334C74C7", 16);
    /**
     * 坐标y
     */
    BigInteger SM2_GY = new BigInteger(
            "BC3736A2" + "F4F6779C" + "59BDCEE3" + "6B692153" + "D0A9877C" + "C62A4740" + "02DF32E5" + "2139F0A0", 16);


    /**
     * 加密名称：SM2
     */
    String ENCODE_NAME_SM2 = "SM2";
    /**
     * 加密名称：SM3
     */
    String ENCODE_NAME_SM3 = "SM3";
    /**
     * 加密名称：SM4
     */
    String ENCODE_NAME_SM4 = "SM4";

    /**
     * 加密算法/分组加密模式/分组填充方式 ：SM4
     */
    String ALGORITHM_NAME_ECB_PADDING_SM4 = "SM4/ECB/PKCS5Padding";


}
