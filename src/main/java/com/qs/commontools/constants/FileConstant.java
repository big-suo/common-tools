package com.qs.commontools.constants;

/**
 * 常量接口
 * @author qiusuo
 * @since 2021-1-23
 */
public interface FileConstant extends CommonConstant {

    /**
     * 文件后缀:xls
     */
    String FILE_SUFFIX_XLS = "xls";

    /**
     * 文件后缀:xlsx
     */
    String FILE_SUFFIX_XLSX = "xlsx";

    /**
     * 常量字符串:GB
     */
    String STR_GB = "GB";

    /**
     * 常量字符串:MB
     */
    String STR_MB = "MB";

    /**
     * 常量字符串:KB
     */
    String STR_KB = "KB";

    /**
     * 常量字符串:B
     */
    String STR_B = "B";

    /**
     * 常量字符串:0B
     */
    String STR_0B = "0B";

    /**
     * 常量字符串:""
     */
    String STR_EMPTY = "";

    /**
     * DECIMAL匹配
     */
    String DECIMAL_FORMAT = "###.0";

    /**
     * 通用字符串
     */
    String CATCH_ERR = "异常捕获";

}
