package com.qs.commontools.constants;

/**
 * http实体对象专用产量接口
 * @author qiusuo
 * @since 2021-09-03
 */
public interface HttpToolConstant extends CommonConstant {



    /**
     * 字符:?
     */
    String CHAR_QUSTION = "\\?";
    /**
     * 字符: &amp;
     */
    String CHAR_AND = "\\&";
    /**
     * 字符:=
     */
    String CHAR_EQUAL = "\\=";

    /**
     * CONTENT_TYPE类型:application/x-www-form-urlencoded
     */
    String CONTENT_TYPE_APPLICATION_WWW_FORM = "application/x-www-form-urlencoded; charset=utf-8";

    /**
     * CONTENT_TYPE类型:application/json
     */
    String CONTENT_TYPE_APPLICATION_JSON_Format = "application/json; charset=UTF-8";

    /**
     * CONTENT_TYPE类型:application/json
     */
    String CONTENT_TYPE_APPLICATION_JSON = "application/json";

    /**
     * CONTENT_TYPE类型:application/x-javascript
     */
    String CONTENT_TYPE_APPLICATION_JAVASCRIPT = "application/x-javascript";

    /**
     * CONTENT_TYPE类型:application/x-msdownload
     */
    String CONTENT_TYPE_APPLICATION_MSDOWNLOAD = "application/x-msdownload";

    /**
     * CONTENT_TYPE类型:application/msword
     */
    String CONTENT_TYPE_APPLICATION_MSWORD = "application/msword";

    /**
     * CONTENT_TYPE类型:aplication/zip
     */
    String CONTENT_TYPE_APPLICATION_ZIP = "aplication/zip";

    /**
     * CONTENT_TYPE类型:application/pdf
     */
    String CONTENT_TYPE_APPLICATION_PDF = "application/pdf";

    /**
     * CONTENT_TYPE类型:multipart/form-data
     */
    String CONTENT_TYPE_FORM_DATA = "multipart/form-data";

    /**
     * CONTENT_TYPE类型:text/plain
     */
    String CONTENT_TYPE_TEXT_PLAIN = "text/plain";

    /**
     * CONTENT_TYPE类型:text/html
     */
    String CONTENT_TYPE_TEXT_HTML = "text/html";

    /**
     * CONTENT_TYPE类型:text/css
     */
    String CONTENT_TYPE_TEXT_CSS = "text/css";

    /**
     * CONTENT_TYPE类型:text/xml
     */
    String CONTENT_TYPE_TEXT_XML = "text/xml";

    /**
     * CONTENT_TYPE类型:text/csv
     */
    String CONTENT_TYPE_TEXT_CSV = "text/csv";

    /**
     * CONTENT_TYPE类型:binary
     */
    String CONTENT_TYPE_BINARY = "binary";

    /**
     * CONTENT_TYPE类型:image/gif
     */
    String CONTENT_TYPE_IMAGE_GIF = "image/gif";

    /**
     * CONTENT_TYPE类型:image/jpeg
     */
    String CONTENT_TYPE_IMAGE_JPEG = "image/jpeg";

    /**
     * CONTENT_TYPE类型:image/png
     */
    String CONTENT_TYPE_IMAGE_PNG = "image/png";

    /**
     * CONTENT_TYPE类型:video/avi
     */
    String CONTENT_TYPE_VIDEO_AVI = "video/avi";

    /**
     * CONTENT_TYPE类型:video/mp4
     */
    String CONTENT_TYPE_VIDEO_MP4 = "video/mp4";

    /**
     * CONTENT_TYPE类型:video/mpg
     */
    String CONTENT_TYPE_VIDEO_MPG = "video/mpg";

// todo 请求方式

    /**
     * 请求方式:GET
     */
    String REQUEST_METHOD_GET = "GET";

    /**
     * 请求方式:POST
     */
    String REQUEST_METHOD_POST = "POST";

    /**
     * 请求方式:HEAD
     */
    String REQUEST_METHOD_HEAD = "HEAD";

    /**
     * 请求方式:PUT
     */
    String REQUEST_METHOD_PUT = "PUT";

    /**
     * 请求方式:DELETE
     */
    String REQUEST_METHOD_DELETE = "DELETE";

    /**
     * 请求方式:CONNECT
     */
    String REQUEST_METHOD_CONNECT = "CONNECT";

    /**
     * 请求方式:OPTIONS
     */
    String REQUEST_METHOD_OPTIONS = "OPTIONS";

    /**
     * 请求方式:TRACE
     */
    String REQUEST_METHOD_TRACE = "TRACE";

// todo accept类型

    /**
     * accept类型：所有
     */
    String ACCEPT_ALL = "*/*";

    /**
     * accept类型：text/xml
     */
    String ACCEPT_TEXT_XML = "text/xml";

    /**
     * accept类型：application/json
     */
    String ACCETP_APPLICATION_JSON = "application/json";

// todo connection类型

    /**
     * connection类型：Keep-Alive
     */
    String CONNECTION_OPEN = "Keep-Alive";

    /**
     * connection类型：close
     */
    String CONNECTION_CLOSE = "close";

// todo http常用字符

    /**
     * http常用字符：--
     */
    String PREFIX = "--";

    /**
     * http常用字符：换行
     */
    String LINEND = "\r\n";

    /**
     * http常用字符：https
     */
    String HTTP_STR_HTTPS = "https";

    /**
     * http常用字符：form-data
     */
    String HTTP_STR_FORM_DATA = "form-data";

    /**
     * http常用字符：Content-type
     */
    String HTTP_STR_CONTENT_TYPE = "Content-Type";

    /**
     * http常用字符：Charset
     */
    String HTTP_STR_CHARSET = "Charset";

    /**
     * http常用字符：Accept
     */
    String HTTP_STR_ACCEPT = "Accept";

    /**
     * http常用字符：Connection
     */
    String HTTP_STR_Connect = "Connection";

    /**
     *  http常用字符：User-Agent
     */
    String HTTP_STR_USER_AGENT = "User-Agent";

//todo 状态码
    // --- 1xx Informational ---

    /** 状态码{@code 100 Continue} (HTTP/1.1 - RFC 2616) */
    public static final int SC_CONTINUE = 100;
    /** 状态码{@code 101 Switching Protocols} (HTTP/1.1 - RFC 2616)*/
    public static final int SC_SWITCHING_PROTOCOLS = 101;
    /** 状态码{@code 102 Processing} (WebDAV - RFC 2518) */
    public static final int SC_PROCESSING = 102;

    // --- 2xx Success ---

    /** 状态码{@code 200 OK} (HTTP/1.0 - RFC 1945) */
    public static final int SC_OK = 200;
    /** 状态码{@code 201 Created} (HTTP/1.0 - RFC 1945) */
    public static final int SC_CREATED = 201;
    /** 状态码{@code 202 Accepted} (HTTP/1.0 - RFC 1945) */
    public static final int SC_ACCEPTED = 202;
    /** 状态码{@code 203 Non Authoritative Information} (HTTP/1.1 - RFC 2616) */
    public static final int SC_NON_AUTHORITATIVE_INFORMATION = 203;
    /** 状态码{@code 204 No Content} (HTTP/1.0 - RFC 1945) */
    public static final int SC_NO_CONTENT = 204;
    /** 状态码{@code 205 Reset Content} (HTTP/1.1 - RFC 2616) */
    public static final int SC_RESET_CONTENT = 205;
    /** 状态码{@code 206 Partial Content} (HTTP/1.1 - RFC 2616) */
    public static final int SC_PARTIAL_CONTENT = 206;
    /**
     * 状态码{@code 207 Multi-Status} (WebDAV - RFC 2518)
     * or
     * 状态码{@code 207 Partial Update OK} (HTTP/1.1 - draft-ietf-http-v11-spec-rev-01?)
     */
    public static final int SC_MULTI_STATUS = 207;

    // --- 3xx Redirection ---

    /** 状态码{@code 300 Mutliple Choices} (HTTP/1.1 - RFC 2616) */
    public static final int SC_MULTIPLE_CHOICES = 300;
    /** 状态码{@code 301 Moved Permanently} (HTTP/1.0 - RFC 1945) */
    public static final int SC_MOVED_PERMANENTLY = 301;
    /** 状态码{@code 302 Moved Temporarily} (Sometimes {@code Found}) (HTTP/1.0 - RFC 1945) */
    public static final int SC_MOVED_TEMPORARILY = 302;
    /** 状态码{@code 303 See Other} (HTTP/1.1 - RFC 2616) */
    public static final int SC_SEE_OTHER = 303;
    /** 状态码{@code 304 Not Modified} (HTTP/1.0 - RFC 1945) */
    public static final int SC_NOT_MODIFIED = 304;
    /** 状态码{@code 305 Use Proxy} (HTTP/1.1 - RFC 2616) */
    public static final int SC_USE_PROXY = 305;
    /** 状态码{@code 307 Temporary Redirect} (HTTP/1.1 - RFC 2616) */
    public static final int SC_TEMPORARY_REDIRECT = 307;

    // --- 4xx Client Error ---

    /** 状态码{@code 400 Bad Request} (HTTP/1.1 - RFC 2616) */
    public static final int SC_BAD_REQUEST = 400;
    /** 状态码{@code 401 Unauthorized} (HTTP/1.0 - RFC 1945) */
    public static final int SC_UNAUTHORIZED = 401;
    /** 状态码{@code 402 Payment Required} (HTTP/1.1 - RFC 2616) */
    public static final int SC_PAYMENT_REQUIRED = 402;
    /** 状态码{@code 403 Forbidden} (HTTP/1.0 - RFC 1945) */
    public static final int SC_FORBIDDEN = 403;
    /** 状态码{@code 404 Not Found} (HTTP/1.0 - RFC 1945) */
    public static final int SC_NOT_FOUND = 404;
    /** 状态码{@code 405 Method Not Allowed} (HTTP/1.1 - RFC 2616) */
    public static final int SC_METHOD_NOT_ALLOWED = 405;
    /** 状态码{@code 406 Not Acceptable} (HTTP/1.1 - RFC 2616) */
    public static final int SC_NOT_ACCEPTABLE = 406;
    /** 状态码{@code 407 Proxy Authentication Required} (HTTP/1.1 - RFC 2616)*/
    public static final int SC_PROXY_AUTHENTICATION_REQUIRED = 407;
    /** 状态码{@code 408 Request Timeout} (HTTP/1.1 - RFC 2616) */
    public static final int SC_REQUEST_TIMEOUT = 408;
    /** 状态码{@code 409 Conflict} (HTTP/1.1 - RFC 2616) */
    public static final int SC_CONFLICT = 409;
    /** 状态码{@code 410 Gone} (HTTP/1.1 - RFC 2616) */
    public static final int SC_GONE = 410;
    /** 状态码{@code 411 Length Required} (HTTP/1.1 - RFC 2616) */
    public static final int SC_LENGTH_REQUIRED = 411;
    /** 状态码{@code 412 Precondition Failed} (HTTP/1.1 - RFC 2616) */
    public static final int SC_PRECONDITION_FAILED = 412;
    /** 状态码{@code 413 Request Entity Too Large} (HTTP/1.1 - RFC 2616) */
    public static final int SC_REQUEST_TOO_LONG = 413;
    /** 状态码{@code 414 Request-URI Too Long} (HTTP/1.1 - RFC 2616) */
    public static final int SC_REQUEST_URI_TOO_LONG = 414;
    /** 状态码{@code 415 Unsupported Media Type} (HTTP/1.1 - RFC 2616) */
    public static final int SC_UNSUPPORTED_MEDIA_TYPE = 415;
    /** 状态码{@code 416 Requested Range Not Satisfiable} (HTTP/1.1 - RFC 2616) */
    public static final int SC_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    /** 状态码{@code 417 Expectation Failed} (HTTP/1.1 - RFC 2616) */
    public static final int SC_EXPECTATION_FAILED = 417;

    /**
     * 状态码Static constants for a 418 error.
     * {@code 418 Unprocessable Entity} (WebDAV drafts?)
     * or {@code 418 Reauthentication Required} (HTTP/1.1 drafts?)
     */
    // not used
    // public static final int SC_UNPROCESSABLE_ENTITY = 418;

    /**
     * 状态码 Static constants for a 419 error.
     * {@code 419 Insufficient Space on Resource}
     * (WebDAV - draft-ietf-webdav-protocol-05?)
     * or {@code 419 Proxy Reauthentication Required}
     * (HTTP/1.1 drafts?)
     */
    public static final int SC_INSUFFICIENT_SPACE_ON_RESOURCE = 419;
    /**
     * 状态码 Static constants for a 420 error.
     * {@code 420 Method Failure}
     * (WebDAV - draft-ietf-webdav-protocol-05?)
     */
    public static final int SC_METHOD_FAILURE = 420;
    /** 状态码{@code 422 Unprocessable Entity} (WebDAV - RFC 2518) */
    public static final int SC_UNPROCESSABLE_ENTITY = 422;
    /** 状态码{@code 423 Locked} (WebDAV - RFC 2518) */
    public static final int SC_LOCKED = 423;
    /** 状态码{@code 424 Failed Dependency} (WebDAV - RFC 2518) */
    public static final int SC_FAILED_DEPENDENCY = 424;

    // --- 5xx Server Error ---

    /** 状态码{@code 500 Server Error} (HTTP/1.0 - RFC 1945) */
    public static final int SC_INTERNAL_SERVER_ERROR = 500;
    /** 状态码{@code 501 Not Implemented} (HTTP/1.0 - RFC 1945) */
    public static final int SC_NOT_IMPLEMENTED = 501;
    /** 状态码{@code 502 Bad Gateway} (HTTP/1.0 - RFC 1945) */
    public static final int SC_BAD_GATEWAY = 502;
    /** 状态码{@code 503 Service Unavailable} (HTTP/1.0 - RFC 1945) */
    public static final int SC_SERVICE_UNAVAILABLE = 503;
    /** 状态码{@code 504 Gateway Timeout} (HTTP/1.1 - RFC 2616) */
    public static final int SC_GATEWAY_TIMEOUT = 504;
    /** 状态码{@code 505 HTTP Version Not Supported} (HTTP/1.1 - RFC 2616) */
    public static final int SC_HTTP_VERSION_NOT_SUPPORTED = 505;

    /** 状态码{@code 507 Insufficient Storage} (WebDAV - RFC 2518) */
    public static final int SC_INSUFFICIENT_STORAGE = 507;

}
