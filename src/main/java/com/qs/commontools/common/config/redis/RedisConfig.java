package com.qs.commontools.common.config.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Redis自动配置类
 * @author qiusuo
 * @since 2021-10-08
 * * @EnableCaching 开启注解式缓存
 */
@Configuration
//@EnableAutoConfiguration
public class RedisConfig{
    @Autowired
    private LettuceConnectionFactory redisConnectionFactory;
//    private RedisConnectionFactory redisConnectionFactory;


//    private RedisProperty redisProperty;
//
//    private RedisPoolProperty redisPoolProperty;
//
//    @Autowired
//    public RedisConfig(RedisProperty redisProperty, RedisPoolProperty redisPoolProperty) {
//        System.out.println(redisPoolProperty.toString());
//        System.out.println(redisProperty.toString());
//        this.redisProperty = redisProperty;
//        this.redisPoolProperty = redisPoolProperty;
//    }

    @Bean
    @SuppressWarnings("all")
//    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
    public RedisTemplate<String, Object> redisTemplate() {
        // 为了开发方面泛型使用e<String, Object>
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        // 连接工程（这一步都一样）
        template.setConnectionFactory(redisConnectionFactory);

        // 配置具体的序列化方式

        // 使用json解析所有的对象，json的序列化方式
        // 使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值（默认使用JDK的序列化方式）
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class);

        // 转译
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);

        // string的序列化方式
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();


        // key采用String的序列化方式
        template.setKeySerializer(stringRedisSerializer);
        // hash的key采用String的序列化方式
        template.setHashKeySerializer(stringRedisSerializer);
        // value的序列化方式采用jackson
        template.setValueSerializer(jackson2JsonRedisSerializer);
        // hash的value的序列化方式也采用jackson
        template.setHashValueSerializer(jackson2JsonRedisSerializer);

        // 设置配置（一样）
        template.afterPropertiesSet();
        return template;
    }

//    @Bean
//    public StringRedisTemplate stringRedisTemplate() {
//        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
//        stringRedisTemplate.setConnectionFactory(redisConnectionFactory);
//        return stringRedisTemplate;
//    }
//
//    @Bean
//    public RedisConnectionFactory redisConnectionFactory() {
//        // 连接池
//        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
//        // 最大空闲连接数, 默认8个
//        jedisPoolConfig.setMaxIdle(redisPoolProperty.getMaxIdleConnections());
//        // 最小空闲连接数, 默认0
//        jedisPoolConfig.setMinIdle(redisPoolProperty.getMinIdleConnections());
//        // 最大连接数, 默认8个
//        jedisPoolConfig.setMaxTotal(redisPoolProperty.getMaxConnections());
//        jedisPoolConfig.setTestOnBorrow(true);
//        jedisPoolConfig.setTestOnReturn(true);
//        // 在空闲时检查有效性, 默认false
//        jedisPoolConfig.setTestWhileIdle(true);
//        jedisPoolConfig.setNumTestsPerEvictionRun(10);
//        jedisPoolConfig.setTimeBetweenEvictionRunsMillis(60000L);
//        // 获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
//        jedisPoolConfig.setMaxWaitMillis(redisPoolProperty.getMaxWaitMillis());
//
//        // redis配置
//        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
//        // 设置ip
//        redisStandaloneConfiguration.setHostName(redisProperty.getHost());
//        // 设置端口
//        redisStandaloneConfiguration.setPort(redisProperty.getPort());
//        // 设置密码
//        redisStandaloneConfiguration.setPassword(redisProperty.getPassword());
//        // 设置连接的数据库
//        redisStandaloneConfiguration.setDatabase(redisProperty.getDatabase());
//
//        // 设置连接池对象
//        JedisClientConfiguration.JedisClientConfigurationBuilder jedisClientConfigurationBuilder = JedisClientConfiguration.builder();
//        JedisClientConfiguration jedisClientConfiguration = jedisClientConfigurationBuilder.usePooling().poolConfig(jedisPoolConfig).build();
//
//        return new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration);
//    }
//
//    @Bean
//    public RedisCacheManager redisCacheManager(RedisConnectionFactory redisConnectionFactory) {
//        return RedisCacheManager.create(redisConnectionFactory);
//    }

}
