package com.qs.commontools.common.service;

import com.alibaba.fastjson.JSON;
import com.qs.commontools.utils.xml.XMLTool;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

import java.util.Iterator;

/**
 * XML处理类
 * @author qiusuo
 * @since 2021-09-11
 */
public class XMLService {

    private static XMLService instance;

    private XMLService() {
    }

    /**
     * 方法同步，调用效率低
     * @return
     */
    public static synchronized XMLService getInstance(){
        if(instance == null){
            instance = new XMLService();
        }
        return instance;
    }

    /**
     * 获取子元素,并设置进封装实体内部
     * @param element
     */
    public XMLTool getChildElement(Element element) {
        XMLTool tool = XMLTool.getInstance();
        // 设置标签名
        tool.setLabelName(element.getName());

        // 设置值
        tool.setLabelValue(element.getText());

        // 属性键值对迭代器
        Iterator<Attribute> attributeIterator = element.attributeIterator();
        while (attributeIterator.hasNext()) {
            Attribute next = attributeIterator.next();
            tool.setLabelAttributes(next.getName(), next.getValue());
        }

        // 获得结点下的子结点迭代器
        Iterator<Element> elementIterator = element.elementIterator();
        while (elementIterator.hasNext()) {
            Element next = elementIterator.next();
            // 递归调用封装子标签对象
            tool.setChilds(getChildElement(next));
        }

        return tool;
    }


    /**
     * 解析Document对象返回解析后的json字符串
     * @param document 文档对象
     * @return 解析后的json字符串
     */
    public String parseDocumentToJSON(Document document) {
        // 获取根结点
        Element rootElt = document.getRootElement();
        // 将结果转为json通用类型赋值结果
        return JSON.toJSONString(getChildElement(rootElt));
    }

    /**
     * 解析Document对象返回XML可操作对象
     *
     * @param document 文档对象
     * @return XML可操作对象
     */
    public XMLTool parseDocument(Document document) {
        // 获取根结点
        Element rootElt = document.getRootElement();
        // 将结果转为json通用类型赋值结果
        return getChildElement(rootElt);
    }
}
