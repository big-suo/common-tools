package com.qs.commontools.common.service;

import java.util.List;

/**
 * 集合排序处理
 * @author qiusuo
 * @since 2021-09-26
 */
public class CollectionSortService {

    /**
     *  检验集合泛型是否可比较 是否继承了Comparable接口
     * @return
     */
    public static boolean checkListGenType(List list) {
        return list.get(0) instanceof Comparable;
    }
}
