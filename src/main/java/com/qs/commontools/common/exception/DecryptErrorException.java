package com.qs.commontools.common.exception;

/**
 * 解密异常。
 * @author qiusuo
 * @since 2021-11-28
 */
public class DecryptErrorException extends Exception{
    public DecryptErrorException() {
    }

    public DecryptErrorException(String message) {
        super(message);
    }

    public DecryptErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public DecryptErrorException(Throwable cause) {
        super(cause);
    }

    public DecryptErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
