package com.qs.commontools.common.exception;

/**
 * 请求方式错误异常
 * @author qiusuo
 * @since 2021-09-08
 */
public class RequestMethodErrorException extends Exception {
    public RequestMethodErrorException() {
    }

    public RequestMethodErrorException(String message) {
        super(message);
    }

    public RequestMethodErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestMethodErrorException(Throwable cause) {
        super(cause);
    }

    public RequestMethodErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
