package com.qs.commontools.common.exception;

/**
 * 长度异常
 * @author qiusuo
 * @since 2021-09-23
 */
public class LengthErrorException extends Exception{
    public LengthErrorException() {
    }

    public LengthErrorException(String message) {
        super(message);
    }

    public LengthErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public LengthErrorException(Throwable cause) {
        super(cause);
    }

    public LengthErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
