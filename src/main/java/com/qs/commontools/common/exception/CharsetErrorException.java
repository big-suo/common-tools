package com.qs.commontools.common.exception;

/**
 * 编码异常
 * @author qiusuo
 * @since 2021-09-08
 */
public class CharsetErrorException extends Exception {
    public CharsetErrorException() {
    }

    public CharsetErrorException(String message) {
        super(message);
    }

    public CharsetErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public CharsetErrorException(Throwable cause) {
        super(cause);
    }

    public CharsetErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
