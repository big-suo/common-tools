package com.qs.commontools.common.exception;

/**
 * 添加水印异常
 * @author qiusuo
 * @since 2021-10-13
 */
public class WaterMarkException extends Exception{
    public WaterMarkException() {
    }

    public WaterMarkException(String message) {
        super(message);
    }

    public WaterMarkException(String message, Throwable cause) {
        super(message, cause);
    }

    public WaterMarkException(Throwable cause) {
        super(cause);
    }

    public WaterMarkException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
