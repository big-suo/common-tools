package com.qs.commontools.common.exception;

/**
 * 日期处理异常
 * @author qiusuo
 * @since 2021-09-29
 */
public class DateHandleException extends Exception {
    public DateHandleException() {
    }

    public DateHandleException(String message) {
        super(message);
    }

    public DateHandleException(String message, Throwable cause) {
        super(message, cause);
    }

    public DateHandleException(Throwable cause) {
        super(cause);
    }

    public DateHandleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
