package com.qs.commontools.common.exception;

/**
 * url异常
 * @author qiusuo
 * @since 2021-09-10
 */
public class URLErrorException extends Exception {
    public URLErrorException() {
    }

    public URLErrorException(String message) {
        super(message);
    }

    public URLErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public URLErrorException(Throwable cause) {
        super(cause);
    }

    public URLErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
