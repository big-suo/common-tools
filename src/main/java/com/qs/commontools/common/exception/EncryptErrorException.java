package com.qs.commontools.common.exception;

/**
 * 加密异常。
 * @author qiusuo
 * @since 2021-11-28
 */
public class EncryptErrorException extends Exception{
    public EncryptErrorException() {
        super();
    }

    public EncryptErrorException(String message) {
        super(message);
    }

    public EncryptErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public EncryptErrorException(Throwable cause) {
        super(cause);
    }

    protected EncryptErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
