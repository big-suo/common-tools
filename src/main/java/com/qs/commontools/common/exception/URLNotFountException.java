package com.qs.commontools.common.exception;

/**
 * url路径未发现异常
 * @author qiusuo
 * @since 2021-09-03
 */
public class URLNotFountException extends Exception {
    public URLNotFountException() {
    }

    public URLNotFountException(String message) {
        super(message);
    }

    public URLNotFountException(String message, Throwable cause) {
        super(message, cause);
    }

    public URLNotFountException(Throwable cause) {
        super(cause);
    }

    public URLNotFountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
