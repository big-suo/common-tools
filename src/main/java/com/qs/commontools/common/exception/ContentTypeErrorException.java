package com.qs.commontools.common.exception;

/**
 * ContentType类型异常
 * @author qiusuo
 * @since 2021-09-07
 */
public class ContentTypeErrorException extends Exception{
    public ContentTypeErrorException() {
    }

    public ContentTypeErrorException(String message) {
        super(message);
    }

    public ContentTypeErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ContentTypeErrorException(Throwable cause) {
        super(cause);
    }

    public ContentTypeErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
