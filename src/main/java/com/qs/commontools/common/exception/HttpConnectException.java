package com.qs.commontools.common.exception;

/**
 * http链接异常
 * @author qiusuo
 * @since 2021-09-03
 */
public class HttpConnectException extends Exception {
    public HttpConnectException() {
    }

    public HttpConnectException(String message) {
        super(message);
    }

    public HttpConnectException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpConnectException(Throwable cause) {
        super(cause);
    }

    public HttpConnectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
