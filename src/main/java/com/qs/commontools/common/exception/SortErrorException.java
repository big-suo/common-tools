package com.qs.commontools.common.exception;

/**
 * 排序异常
 * @author qiusuo
 * @since 2021-09-26
 */
public class SortErrorException extends Exception{
    public SortErrorException() {
    }

    public SortErrorException(String message) {
        super(message);
    }

    public SortErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SortErrorException(Throwable cause) {
        super(cause);
    }

    public SortErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
