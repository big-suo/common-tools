package com.qs.commontools.common.entity.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * redis连接池配置对象
 * @author qiusuo
 * @since 2021-10-08
 */
@Component
@Configuration
@PropertySource(value = {"classpath:application.properties","classpath:application.yml","classpath:application.yaml"},
        encoding = "UTF-8",ignoreResourceNotFound = true)
public class RedisPoolProperty {

    /**
     * 最大等待毫秒数
     */
    @Value("${qs.commontools.cache.maxWaitMillis:}")
    private Long maxWaitMillis = 10000L;
    /**
     * 最大连接数, 默认8个
     */
    @Value("${qs.commontools.cache.maxConnections:}")
    private Integer maxConnections = 8;
    /**
     * 最大空闲连接数, 默认8个
     */
    @Value("${qs.commontools.cache.maxIdleConnections:}")
    private Integer maxIdleConnections = 8;
    /**
     * 最小空闲连接数, 默认0个
     */
    @Value("${qs.commontools.cache.minIdleConnections:}")
    private Integer minIdleConnections = 0;
}
