package com.qs.commontools.common.entity.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Redis配置对象
 * @author qiusuo
 * @since 2021-10-08
 */
@Component
@Configuration
@PropertySource(value = {"classpath:application.properties","classpath:application.yml","classpath:application.yaml"},
encoding = "UTF-8",ignoreResourceNotFound = true)
public class RedisProperty {
    @Value("${qs.commontools.cache.host:}")
    private String host = "127.0.0.1";
    @Value("${qs.commontools.cache.port:}")
    private Integer port = 6379;
    @Value("${qs.commontools.cache.database:}")
    private Integer database = 0;
    @Value("${qs.commontools.cache.password:}")
    private String password;
}
