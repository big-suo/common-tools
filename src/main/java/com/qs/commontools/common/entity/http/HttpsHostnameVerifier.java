package com.qs.commontools.common.entity.http;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * 主机名校验类
 * @author qiusuo
 * @since 2021-08-03
 */
public class HttpsHostnameVerifier implements HostnameVerifier {

//    正例：
//        HostnameVerifier hnv=new HosernameVerifier(){
//        @Override
//        public boolean verify(String hostname,SSLSession session){
//            if("youhostname".equals(hostname)){
//                return true;
//            }else{
//                  HostnameVerifier        hv=HttpsURLConnection.getDefaultHostnameVerifier();
//                 return hv.verify(hostname,session);
//                  }
//          }
//        }
    /**
     * 验证方法
     * @param hostname 主机名
     * @param session  到主机的连接上使用的 SSLSession
     * @return 通用返回true表示主机验证全都通过验证
     */
    @Override
    public boolean verify(String hostname, SSLSession session) {
        // 如果主机名是可接受的，则返回 true
        return true;
    }
}
