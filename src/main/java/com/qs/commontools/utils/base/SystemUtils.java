package com.qs.commontools.utils.base;

import java.awt.*;
import java.io.File;

/**
 * 系统工具类
 * @author qiusuo
 * @since 2021-11-13
 */
public class SystemUtils {

    /**
     * 获得当前项目的路径。
     *
     * @return 项目地址
     */
    public static String getProjectPath(){
        return System.getProperty("user.dir");
    }

    /**
     * 获得用户的主目录。
     *
     * @return 主目录地址
     */
    public static String getHome() {
        return System.getProperty("user.home");
    }


    /**
     * 获取类所在的路径。
     *
     * @param thiz 传入this(表示当前路径)或某个对象(某个对象所在路径)
     * @return 类所在的路径。
     */
    public static String getCurrentPath(Object thiz) {
        return new File(thiz.getClass().getResource("").getPath()).toString();
    }

    /**
     * 获得系统账号名称。
     *
     * @return 账号名称
     */
    public static String getUser() {
        return System.getProperty("user.name");
    }

    /**
     * 获取操作系统名称。
     *
     * @return 系统名称
     */
    public static String getOsName() {
        return System.getProperty("os.name");
    }

    /**
     * 获取操作系统架构。
     *
     * @return 系统架构
     */
    public static String getOsArch() {
        return System.getProperty("os.arch");
    }

    /**
     * 获取操作系统版本。
     *
     * @return 系统版本
     */
    public static String getOsVersion() {
        return System.getProperty("os.version");
    }

    /**
     * 获得文件分隔符，相当于File.separator,（UNIX中是“/”）。
     *
     * @return 文件分隔符
     */
    public static String getSeparatorFile() {
        return System.getProperty("file.separator");
    }

    /**
     * 获得路径分隔符（UNIX中是“:”）。
     *
     * @return 路径分隔符
     */
    public static String getSeparatorPath() {
        return System.getProperty("path.separator");
    }

    /**
     * 获得行分割符（UNIX中是“/n”）。
     *
     * @return 行分割符
     */
    public static String getSeparatorLine() {
        return System.getProperty("line.separator");
    }


    /**
     * 获得该系统上所有可获得的字体名称。
     *
     * @return 字体名称数组
     */
    public static String[] getAvailableFontsName() {
        // 应用程序在特定平台上可用的 GraphicsDevice 对象和 Font 对象的集合
        GraphicsEnvironment grapEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
        // 返回一个包含此 GraphicsEnvironment 中所有字体系列名称的数组
        return grapEnv.getAvailableFontFamilyNames();
    }
}
