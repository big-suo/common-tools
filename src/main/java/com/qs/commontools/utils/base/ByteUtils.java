package com.qs.commontools.utils.base;

/**
 * 字节工具类。
 *
 * @author qiusuo
 * @since 2021-11-29
 */
public class ByteUtils {

    /**
     * 两个字节数组比较是否相等。
     *
     * @param dataa 字节数组1
     * @param datab 字节数组2
     * @return true相等，false不等
     */
    public static boolean isEqual(byte[] dataa, byte[] datab) {
        if (dataa == datab) {
            return true;
        }
        if (dataa == null || datab == null) {
            return false;
        }
        if (dataa.length != datab.length) {
            return false;
        }

        int result = 0;
        // 时间开销为常数
        for (int i = 0; i < dataa.length; i++) {
            // 先异或(相同为0,不同为1),再或(有一个1,就不为0)
            result |= dataa[i] ^ datab[i];
        }
        return result == 0;
    }
}
