package com.qs.commontools.utils.base;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * base64编码和String工具类。
 * @author qiusuo
 * @since 2021-11-24
 */
public class Base64StringTool {

    /**
     * 普通字节数组转 base64 字符串
     * @param bytes 待编码的普通字节数组
     * @return 编码后的base64字符串
     */
    public static String encodeBase64ToStr(byte[] bytes) {
        // return org.apache.commons.codec.binary.Base64.encodeBase64String(bytes)
        return Base64.getEncoder().encodeToString(bytes);
    }

    /**
     * 普通字符串转 base64 字符串
     * @param str 待编码的 普通字符串
     * @return 编码后的base64字符串
     * @throws UnsupportedEncodingException 不支持转码
     */
    public static String encodeBase64ToStr(String str) throws UnsupportedEncodingException {
        // return org.apache.commons.codec.binary.Base64.encodeBase64String(bytes)
        return encodeBase64ToStr(str.getBytes(StandardCharsets.UTF_8));

    }

    /**
     * 普通字节数组转 base64 字节数组
     * @param bytes 待编码的 普通字节数组
     * @return 编码后的base64字节数组
     * @throws UnsupportedEncodingException 不支持转码
     */
    public static byte[] encodeBase64ToByte(byte[] bytes) throws UnsupportedEncodingException {
        return encodeBase64ToStr(bytes).getBytes(StandardCharsets.UTF_8);
    }

    /**
     * 普通字符串转 base64 字节数组
     * @param str 待编码的 普通字符串
     * @return 编码后的base64字节数组
     * @throws UnsupportedEncodingException 不支持转码
     */
    public static byte[] encodeBase64ToByte(String str) throws UnsupportedEncodingException {
        return encodeBase64ToByte(str.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * base64字符串待转成 普通字符串字节数组
     * @param str base64字符串
     * @return 解码后的普通字节数组
     */
    public static byte[] decodeBase64ToByte(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        // return org.apache.commons.codec.binary.Base64.decodeBase64(base64Code)
        return Base64.getDecoder().decode(str);
    }

    /**
     * base64字节数组 转成 普通字节数组
     * @param bytes base64字节数组
     * @return 解码后的普通字节数组
     * @throws UnsupportedEncodingException 不支持转码
     */
    public static byte[] decodeBase64ToByte(byte[] bytes) throws UnsupportedEncodingException {
        String s = new String(bytes, StandardCharsets.UTF_8);
        return decodeBase64ToByte(s);
    }
    /**
     * base64字符串待转成 普通字符串
     * @param str base64字符串
     * @return 解码后的普通字符串
     * @throws UnsupportedEncodingException 不支持转码
     */
    public static String decodeBase64ToStr(String str) throws UnsupportedEncodingException {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        return new String(decodeBase64ToByte(str), StandardCharsets.UTF_8);
    }
    /**
     * base64字节数组 转成 普通字符串
     * @param bytes base64字节数组
     * @return 解码后的普通字符串
     * @throws UnsupportedEncodingException 不支持转码
     */
    public static String decodeBase64ToStr(byte[] bytes) throws UnsupportedEncodingException {
        String s = new String(bytes, StandardCharsets.UTF_8);
        return decodeBase64ToStr(s);
    }

}
