package com.qs.commontools.utils.base;

/**
 * 进制到字节数组和字符串之间的转换工具类。
 * @author qiusuo
 * @since 2021-11-28
 */
public class TransformUtils {
    private static final char[] HEX_CHARS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    /**
     * 字节数组转十六进制字符串。
     *
     * @param bytes 字节数组
     * @return 十六进制字符串
     */
    public static String hexByteToStr(byte[] bytes) {
        String var1 = "";

        for(int var2 = 0; var2 < bytes.length; ++var2) {
            var1 = var1 + HEX_CHARS[bytes[var2] >>> 4 & 15];
            var1 = var1 + HEX_CHARS[bytes[var2] & 15];
        }

        return var1;
    }


    /**
     * 十六进制字符串转字节数组
     * @param hex 十六进制字符串
     * @return 字节数组
     */
    public static byte[] hexStrToByte(String hex) {
        char[] var1 = hex.toUpperCase().toCharArray();
        int var2 = 0;

        for(int var3 = 0; var3 < var1.length; ++var3) {
            if (var1[var3] >= '0' && var1[var3] <= '9' || var1[var3] >= 'A' && var1[var3] <= 'F') {
                ++var2;
            }
        }

        byte[] var6 = new byte[var2 + 1 >> 1];
        int var4 = var2 & 1;

        for(int var5 = 0; var5 < var1.length; ++var5) {
            if (var1[var5] >= '0' && var1[var5] <= '9') {
                var6[var4 >> 1] = (byte)(var6[var4 >> 1] << 4);
                var6[var4 >> 1] = (byte)(var6[var4 >> 1] | var1[var5] - 48);
            } else {
                if (var1[var5] < 'A' || var1[var5] > 'F') {
                    continue;
                }

                var6[var4 >> 1] = (byte)(var6[var4 >> 1] << 4);
                var6[var4 >> 1] = (byte)(var6[var4 >> 1] | var1[var5] - 65 + 10);
            }

            ++var4;
        }

        return var6;
    }
}
