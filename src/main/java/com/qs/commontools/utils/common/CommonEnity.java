package com.qs.commontools.utils.common;

import java.util.Date;

/**
 * 基础数据库实例字段
 * @author qisuuo
 * @since 2021-11-28
 */
public class CommonEnity {
    /**
     * id
     */
    private String id;

    /**
     * 格林威治中央区时:创建时间
     */
    private Date gmtCreate;

    /**
     * 格林威治中央区时:修改时间
     */
    private Date gmtModify;

    /**
     * 逻辑删除位: 0未删除，1已删除
     */
    private Integer deleted;

    /**
     * 乐观锁判断位
     */
    private Integer version;

    /**
     * 获得唯一标识。
     *
     * @return 唯一标识
     */
    public String getId() {
        return id;
    }

    /**
     * 谁唯一标识。
     *
     * @param id 唯一标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获得创建时间。
     *
     * @return 创建时间
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * 设置创建时间。
     *
     * @param gmtCreate 创建时间
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * 获得修改时间。
     *
     * @return 修改时间
     */
    public Date getGmtModify() {
        return gmtModify;
    }

    /**
     * 设置修改时间。
     *
     * @param gmtModify 修改时间
     */
    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    /**
     * 获得删除状态。
     *
     * @return 删除状态
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 设置删除状态。
     *
     * @param deleted 删除状态
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * 获得版本。
     *
     * @return 版本
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 设置版本。
     *
     * @param version 版本
     */
    public void setVersion(Integer version) {
        this.version = version;
    }
}
