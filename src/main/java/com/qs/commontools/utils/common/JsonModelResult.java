package com.qs.commontools.utils.common;


import com.qs.commontools.utils.base.StringUtils;

import java.io.Serializable;

/**
 * Json类型结果模版
 * @author qiusuo
 * @since 2021-09-02
 */
public class JsonModelResult implements Serializable {

    private static final long serialVersionUID = 2942763247431129605L;

    /**
     * 是否请求成功
     */
    private Boolean success;

    /**
     * 请求状态码
     */
    private Integer code;

    /**
     * 请求信息
     */
    private String msg;

    /**
     * 请求数据
     */
    private Object data;

    /**
     * 请求序号
     */
    private String tranceId;

    private JsonModelResult() {
    }

    /**
     * 获得结果实例
     * @return 实例
     */
    public static JsonModelResult getInstance() {
        JsonModelResult jsonModelResult = new JsonModelResult();
        jsonModelResult.setTranceId(StringUtils.getSimpleUUID());
        jsonModelResult.setData(null);
        jsonModelResult.setSuccess(null);
        jsonModelResult.setMsg(null);
        jsonModelResult.setCode(null);
        return jsonModelResult;
    }

    /**
     * 获取请求状态
     * @return 请求状态
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * 设置请求状态
     * @param success 请求状态
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * 获取请求信息。
     * @return 信息
     */
    public String getMsg() {
        return msg;
    }

    /**
     * 设置请求信息。
     * @param msg 信息
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 获得结果数据。
     * @return 结果值
     */
    public Object getData() {
        return data;
    }

    /**
     * 设置结果数据。
     * @param data 结果值
     */
    public void setData(Object data) {
        this.data = data;
    }

    /**
     * 获取请求序号
     * @return UUID序号
     */
    public String getTranceId() {
        return tranceId;
    }

    /**
     * 设置请求序号
     * @param tranceId 请求序号
     */
    private void setTranceId(String tranceId) {
        this.tranceId = tranceId;
    }

    /**
     * 获取请求状态码
     * @return 状态码
     */
    public Integer getCode() {
        return code;
    }

    /**
     * 设置请求状态码。
     * @param code 状态码
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * 获得序列号。
     * @return 序列号
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * toString。
     *
     * @return 对象字符串
     */
    @Override
    public String toString() {
        return "JsonModelResult{" +
                "success=" + success +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", tranceId='" + tranceId + '\'' +
                '}';
    }
}
