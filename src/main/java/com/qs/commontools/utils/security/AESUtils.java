package com.qs.commontools.utils.security;

import com.qs.commontools.common.service.AesService;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorConfig;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

/**
 * AES加解密工具类
 * @author qiusuo
 * @since 2021-09-22
 */
public class AESUtils {

    /**
     * 创建16位密钥或盐度值,形如 "W7HZY7FXZH7LAEHQ"，注意：调用该方法需要引入新依赖。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/AESTest.java
     *
     * @return 16位随机生成数
     */
    public static String createKey() {
        GoogleAuthenticatorConfig.GoogleAuthenticatorConfigBuilder var = new GoogleAuthenticatorConfig.GoogleAuthenticatorConfigBuilder();
        var.setTimeStepSizeInMillis(30000L);
        GoogleAuthenticator var0 = new GoogleAuthenticator(var.build());
        GoogleAuthenticatorKey var1 = var0.createCredentials();
        return var1.getKey();
    }

//    /**
//     * 随机生成密钥
//     *
//     * @return
//     */
//    public static String getAESRandomKey() {
//        SecureRandom random = new SecureRandom();
//        long randomKey = random.nextLong();
//        return String.valueOf(randomKey);
//    }

    /**
     * AES 加密操作 ECB模式加密，没有盐度值入参。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/AESTest.java
     *
     * @param content 待加密内容
     * @param key     加密密钥
     * @return 返回Base64转码后的加密数据
     */
    public static String encrypt(String content, String key) {
        AesService instance = AesService.getInstance();
        return instance.encryptECB(content,key);
    }

    /**
     * AES 加密操作 CBC模式加密，有盐度值入参。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/AESTest.java
     *
     * @param content 待加密内容
     * @param key 加密密钥
     * @param iv 偏移量，盐度值
     * @return 返回Base64转码后的加密数据
     */
    public static String encrypt(String content, String key, String iv) {
        AesService instance = AesService.getInstance();
        return instance.encryptCBC(content, key, iv);
    }

    /**
     * AES 解密操作 ECB模式，没有盐度值入参。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/AESTest.java
     *
     * @param content 待解密内容
     * @param key 解密密钥
     * @return 解密后的字符串结果
     */
    public static String decrypt(String content, String key) {
        AesService instance = AesService.getInstance();
        return instance.decryptECB(content, key);
    }

    /**
     * AES 解密操作 CBC模式，有盐度值入参。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/AESTest.java
     *
     * @param content 待解密内容
     * @param key 解密密钥
     * @param iv 偏移量盐度值
     * @return 解密后的字符串结果
     */
    public static String decrypt(String content, String key, String iv) {
        AesService instance = AesService.getInstance();
        return instance.decryptCBC(content, key, iv);
    }



}
