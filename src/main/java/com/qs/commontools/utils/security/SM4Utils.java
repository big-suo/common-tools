package com.qs.commontools.utils.security;

import com.qs.commontools.constants.SecurityConstant;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Arrays;

/**
 * SM2加解密工具类。
 * <br> SM4 无线局域网标准的分组数据算法。对称加密，密钥长度和分组长度均为128位。
 *
 * @author qiusuo
 * @since 2021-11-28
 */
public class SM4Utils {
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private static final String ENCODING = SecurityConstant.CODE_FORMAT_UTF_8;

    private static final String ALGORITHM_NAME = SecurityConstant.ENCODE_NAME_SM4;

    /**
     * 加密算法/分组加密模式/分组填充方式
     * PKCS5Padding-以8个字节为一组进行分组加密
     */
    private static final String ALGORITHM_NAME_ECB_PADDING = SecurityConstant.ALGORITHM_NAME_ECB_PADDING_SM4;

    /**
     * 128-32位16进制；256-64位16进制
     */
    private static final int DEFAULT_KEY_SIZE = SecurityConstant.NUM_INTEGER_ONE_HUNDRED_AND_TWENTY_EIGHT;

    /**
     * 生成ECB暗号。
     *
     * @param algorithmName 算法名称
     * @param mode 模式
     * @param key 密钥
     * @return 暗号
     * @throws Exception 异常
     */
    private static Cipher generateEcbCipher(String algorithmName, int mode, byte[] key) throws Exception {
        Cipher cipher = Cipher.getInstance(algorithmName, BouncyCastleProvider.PROVIDER_NAME);
        Key sm4Key = new SecretKeySpec(key, ALGORITHM_NAME);
        cipher.init(mode, sm4Key);
        return cipher;
    }

    /**
     * 自动生成密钥，默认32位。
     *
     * @return 密钥
     * @throws Exception 生成密钥异常
     */
    public static String createKey() throws Exception {
        return createKey(DEFAULT_KEY_SIZE);
    }

    /**
     * 自动生成密钥，128-32位16进制；256-64位16进制。
     *
     * @param keySize 密钥长度，可输入128或256
     * @return 密钥
     * @throws Exception 生成密钥异常
     */
    public static String createKey(int keySize) throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM_NAME, BouncyCastleProvider.PROVIDER_NAME);
        keyGenerator.init(keySize, new SecureRandom());
        return ByteUtils.toHexString(keyGenerator.generateKey().getEncoded());
    }

    /**
     * sm4加密，ECB模式。
     * <br> 密文长度不固定，会随着被加密字符串长度的变化而变化
     *
     * @param key 16进制密钥（忽略大小写）
     * @param str 待加密字符串
     * @return  返回16进制的加密字符串
     * @throws Exception 加密异常
     */
    public static String encryptEcb(String key, String str) throws Exception {
        // 16进制字符串-->byte[]
        byte[] keyData = ByteUtils.fromHexString(key);
        // String-->byte[]
        byte[] srcData = str.getBytes(ENCODING);
        // 加密后的数组
        Cipher cipher = generateEcbCipher(ALGORITHM_NAME_ECB_PADDING, Cipher.ENCRYPT_MODE, keyData);
        byte[] cipherArray = cipher.doFinal(srcData);
        // byte[]-->hexString
        return ByteUtils.toHexString(cipherArray);
    }

    /**
     * sm4解密，ECB模式。
     *
     * @param key 16进制密钥
     * @param str 16进制的加密字符串（忽略大小写）
     * @return 解密后的字符串
     * @throws Exception 解密异常
     */
    public static String decryptEcb(String key, String str) throws Exception {
        //// hexString-->byte[]
        byte[] keyData = ByteUtils.fromHexString(key);
        byte[] cipherData = ByteUtils.fromHexString(str);
        byte[] srcData = decrypt_Ecb_Padding(keyData, cipherData);
        return new String(srcData, ENCODING);
    }

    /**
     * 校验加密前后的字符串是否为同一数据。
     *
     * @param key 16进制密钥（忽略大小写）
     * @param ciphertext 16进制加密后的字符串
     * @param Plaintext 加密前的字符串
     * @return true 一致， false 不一致
     * @throws Exception 加密前后校验异常
     */
    public static boolean verifyEcb(String key, String ciphertext, String Plaintext) throws Exception {
        // hexString-->byte[]
        byte[] keyData = ByteUtils.fromHexString(key);
        // 将16进制字符串转换成数组
        byte[] cipherData = ByteUtils.fromHexString(ciphertext);
        // 解密
        byte[] decryptData = decrypt_Ecb_Padding(keyData, cipherData);
        // 将原字符串转换成byte[]
        byte[] srcData = Plaintext.getBytes(ENCODING);
        return Arrays.equals(decryptData, srcData);
    }


    /**
     * 解密ECB
     */
    private static byte[] decrypt_Ecb_Padding(byte[] key, byte[] cipherText) throws Exception {
        Cipher cipher = generateEcbCipher(ALGORITHM_NAME_ECB_PADDING, Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(cipherText);
    }
}
