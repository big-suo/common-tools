package com.qs.commontools.utils.security;

import com.qs.commontools.constants.CommonConstant;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5加解密。
 * MD5算法不可逆。
 * @author qiusuo
 * @since 2021-11-26
 */
public class MD5Utils {

    /**
     * 加密。
     * @param str 加密字符串
     * @return 加密结果
     */
    public static String encrypt(String str) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] digest = md5.digest(str.getBytes(CommonConstant.CODE_FORMAT_UTF_8));
            String s = ByteUtils.toHexString(digest);
            return s;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }


}
