package com.qs.commontools.utils.security;


import com.qs.commontools.utils.base.StringUtils;

/**
 * 安全工具类
 * @author qiusuo
 * @since 2021-10-27
 */
public class SecurityUtils {

    /**
     * 脱敏名称，只第一个字符保持明码，其他全用 "*" 代替。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/SecurityUtilsTest.java
     *
     * @param fullName 需要被脱敏的名称
     * @return 脱敏后结果
     */
    public static String desensitizedName(String fullName){
        if (!StringUtils.isBlank(fullName)){
            String name = StringUtils.left(fullName, 1);
            return StringUtils.rightPad(name, StringUtils.length(fullName), "*");
        }
        return fullName;
    }

    /**
     * 脱敏手机号码，默认前三位后四位明码，中间五位为 "*"。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/SecurityUtilsTest.java
     *
     * @param phoneNumber 需要被脱敏的手机号
     * @return 脱敏后结果
     */
    public static String desensitizedPhoneNumber(String phoneNumber){
        return desensitizedPhoneNumber(phoneNumber,3,4);
    }

    /**
     * 脱敏手机号码，自定义前后保留明码个数，中间填充 "*"。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/SecurityUtilsTest.java
     *
     * @param phoneNumber 需要被脱敏的手机号
     * @param start 前面保留几位明码
     * @param end 后面保留几位明码
     * @return 脱敏后结果
     */
    public static String desensitizedPhoneNumber(String phoneNumber, Integer start, Integer end){
        if (start + end >= phoneNumber.length()) {
            return phoneNumber;
        }
        if(!StringUtils.isBlank(phoneNumber)){
            int num = phoneNumber.length() - start - end;
            num = num > 0 ? num : 0;
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < num; i++) {
                stringBuffer.append("*");
            }
            phoneNumber = phoneNumber.replaceAll("(\\w{" + start + "})\\w*(\\w{" + end + "})", "$1" + stringBuffer.toString() + "$2");
        }
        return phoneNumber;
    }

    /**
     * 脱敏身份证号码，默认保留前后各一位明码，中间填充为 "*"。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/SecurityUtilsTest.java
     *
     * @param idNumber 需要被脱敏的身份证
     * @return 脱敏后结果
     */
    public static String desensitizedIdNumber(String idNumber){
        return desensitizedIdNumber(idNumber,1,1);
    }

    /**
     * 脱敏身份证号码，自定义前后保留明码个数，中间填充 "*"。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/SecurityUtilsTest.java
     *
     * @param idNumber 需要被脱敏的身份证
     * @param start 前面保留几位明码
     * @param end 后面保留几位明码
     * @return 脱敏后结果
     */
    public static String desensitizedIdNumber(String idNumber, Integer start, Integer end){
        if (start + end >= idNumber.length()) {
            return idNumber;
        }
        if (!StringUtils.isBlank(idNumber)) {
            int num = idNumber.length() - start - end;

            if (idNumber.length() == 15){
                idNumber = idNumber.replaceAll("(\\w{" + start + "})\\w*(\\w{" + end + "})", "$1" + padStr(num) + "$2");
            }
            if (idNumber.length() == 18){
                idNumber = idNumber.replaceAll("(\\w{" + start + "})\\w*(\\w{" + end + "})", "$1" + padStr(num) + "$2");
            }
        }

//        if (!StringUtils.isBlank(idNumber)) {
//            return StringUtils.left(idNumber, 6).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(idNumber, 3), StringUtils.length(idNumber), "*"), "******"));
//        }
        return idNumber;
    }

    /**
     * 脱敏地址，将第四到十一个字符用"*"代替。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/security/SecurityUtilsTest.java
     *
     * @param address 需要被脱敏的地址
     * @return 脱敏后结果
     */
    public static String desensitizedAddress(String address){
        if (!StringUtils.isBlank(address)) {
            return StringUtils.left(address, 3).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(address, address.length()-11), StringUtils.length(address), "*"), "***"));
        }
        return address;
    }

    private static String padStr(Integer num){
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < num; i++) {
            stringBuffer.append("*");
        }
        return stringBuffer.toString();
    }

}
