package com.qs.commontools.utils.security;

/**
 * SM1加解密工具类。
 * <br> SM1 为对称加密。其加密强度与AES相当。该算法不公开，调用该算法时，需要通过加密芯片的接口进行调用。
 *
 * @author qiusuo
 * @since 2021-11-28
 */
public class SM1Utils {

}
