package com.qs.commontools.utils.collection;


import com.qs.commontools.utils.base.StringUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 集合Collection工具类
 * @author qiusuo
 * @since 2021-09-26
 */
public class ListUtils {
    // 参考文档：https://www.cnblogs.com/yw0219/p/7222108.html?utm_source=itdadao&utm_medium=referral

//    /**
//     * 排序方法 默认从小到大排序
//     * @param list 集合类型
//     * @param comparator 排序规则
//     * @throws Exception
//     * @return 排序后的结果
//     */
//    public static List<T> sort(List<T> list, Comparator<T> comparator) throws Exception {
//        // 校验排序集合
//        if (null == list) {
//            throw new NullPointerException("排序的集合不能为null");
//        }
//
//        // 如果长度为0不用排序
//        if (list.size() == 0) {
//            return list;
//        }
//        // 检验集合泛型是否可比较
//        boolean t = CollectionSortService.checkListGenType(list);
//        // 集合泛型为Integer或者String已经实现Comparable接口的类
//        if (t) {
////            Collections.sort(list,new Comparator<T>(){});
//        } else {
//            throw new SortErrorException("集合元素不全部能排序，可能存在元素没实现Comparable接口，或者给sort方法传入排序规则参数");
//        }
////        Collections.sort(list,comparator);
//        return list;
//    }

    /**
     * 深拷贝list。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/collection/CollectionTest.java
     *
     * @param list 需要拷贝的集合
     * @return 深拷贝后的结果
     */
    public static List copyList(List list){
        List collect = (List) list.stream().collect(Collectors.toList());
        return collect;
    }

    /**
     * 深拷贝list。
     * <p>跳过前几个进行拷贝</p>
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/collection/CollectionTest.java
     *
     * @param list 需要拷贝的集合
     * @param skip 需要跳过几个索引
     * @return 深拷贝后的结果
     */
    public static List copyList(List list,Integer skip){
        List collect = (List) list.stream().skip(skip).collect(Collectors.toList());
        return collect;
    }

    /**
     * 深拷贝list。
     * <p>可用这种方法实现拷贝时的过滤</p>
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/collection/CollectionTest.java
     *
     * @param stream 需要拷贝的集合流，一般用list.stream()获取
     * @return 深拷贝后的结果
     */
    public static List copyList(Stream stream){
        return (List) stream.collect(Collectors.toList());
    }

    /**
     * 遍历集合，将集合元素以特定符号拼接转为字符串，默认以 "," 号进行拼接，前后不拼接内容。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/collection/CollectionTest.java
     *
     * @param list 入参List集合,一般为泛型Integer、String
     * @return 遍历转为字符串后的结果，如 ("zhangsan","lisi","wangwu")
     */
    public static String foreach(List list) {
        return foreach(list,null,null,null);
    }

    /**
     * 遍历集合，将集合元素以特定符号拼接转为字符串，可设置前后拼接内容和分割的特定符号。
     * <br>
     * <br>示例：https://gitee.com/big-suo/common-tools/blob/develop/src/test/java/com/qs/commontools/collection/CollectionTest.java
     *
     * @param list 入参List集合,一般为泛型Integer、String
     * @param open 拼接结果的前缀，如 "(",不想要前缀可以入参为null或者""
     * @param separator 拼接分隔的特定符号，如","
     * @param close 拼接结果的后缀，如")",不想要前缀可以入参为null或者""
     * @return 遍历转为字符串后的结果，如 ("zhangsan","lisi","wangwu")
     */
    public static String foreach(List list, String open, String separator, String close) {
        if (StringUtils.isBlank(open)) {
            open = "";
        }
        if (StringUtils.isBlank(separator)) {
            separator = ",";
        }
        if (StringUtils.isBlank(close)) {
            close = "";
        }
        String res = list.stream().map(String::valueOf).collect(Collectors.joining(separator)).toString();
        return open + res + close;
    }
}
