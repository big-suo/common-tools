package com.qs.commontools.utils.math;

import java.text.DecimalFormat;

/**
 * 数字工具类
 * @author qiusuo
 * @since 2021-10-25
 */
public class MathUtils {

    /**
     * 将整数化为千分位分割字符串，10000 转 "10,000"。
     * @param num 入参为Integer类型
     * @return 千分位分割字符串
     */
    public static String micrometer(Integer num) {
        return micrometer(num + "");
    }

    /**
     * 将Long类型整数化为千分位分割字符串，10000000000000L 转 "10,000,000,000,000"。
     *
     * @param num 入参为Long类型
     * @return 千分位分割字符串
     */
    public static String micrometer(Long num) {
        return micrometer(num + "");
    }

    /**
     * 将整数字符串化为千分位分割字符串，"10000" 转 "10,000"。
     *
     * @param num 入参为String类型
     * @return 千分位分割字符串
     */
    public static String micrometer(String num) {
        //先将字符串颠倒顺序
        num = new StringBuilder(num).reverse().toString();
        String str2 = "";
        for(int i = 0; i < num.length(); i++){
            if(i*3+3 > num.length()){
                str2 += num.substring(i*3, num.length());
                break;
            }
            str2 += num.substring(i*3, i*3+3)+",";
        }
        if(str2.endsWith(",")){
            str2 = str2.substring(0, str2.length()-1);
        }
        //最后再将顺序反转过来
        return new StringBuilder(str2).reverse().toString();
    }

    /**
     * String转float类型。
     *
     * @param str float类型的字符串
     * @return float类型
     */
    public static float parseFloatValue(String str) {
        return Float.parseFloat(str);
    }

    /**
     * 获取百分比方法，默认不保留小数点。
     *
     * @param i1 被除数（分子）
     * @param i2 除数（分母）
     * @return 百分比结果
     */
    public static float percent(int i1, int i2) {
        return percent(i1, i2, 0);
    }

    /**
     * 获取百分比方法，可设置小数点之后保留几位小数。
     *
     * @param i1 被除数（分子）
     * @param i2 除数（分母）
     * @param size 获得的百分比小数点后几位
     * @return 百分比结果
     */
    public static float percent(int i1, int i2, int size) {
        StringBuffer stringBuffer = new StringBuffer("0.00");
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                stringBuffer.append("0");
            }
        }
        DecimalFormat df = new DecimalFormat(stringBuffer.toString());
        String format = df.format((float) i1 / i2);
        float v = Float.parseFloat(format);
        return v * 100;

//        BigDecimal.valueOf((float)grnum/count).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()
    }
}
