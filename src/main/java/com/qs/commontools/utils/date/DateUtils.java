package com.qs.commontools.utils.date;

import com.qs.commontools.common.exception.DateHandleException;
import com.qs.commontools.utils.regular.RegularUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 * @author qiusuo
 * @since 2021-09-29
 */
public class DateUtils {


    /**
     * 格式化日期，默认规则 yyyy-MM-dd HH:mm:ss。
     *
     * @param date 需要被格式化的日期
     * @return 格式化后的结果字符串
     */
    public static String format(Date date){
        return format(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 格式化日期，限定格式化规则。
     *
     * @param date 需要被格式化的日期
     * @param formatStr 格式规则，如"yyyy-MM-dd HH:mm:ss"
     * @return 格式化后的结果字符串
     */
    public static String format(Date date, String formatStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
        return sdf.format(date);
    }

    /**
     * 将日期字符串转日期类型。
     * <br>默认转换格式 "yyyy-MM-dd HH:mm:ss"
     *
     * @param dateStr 日期规则字符串，如："2021-11-13 16:09:04"
     * @throws ParseException 转换异常
     * @return 转换后的日期对象
     */
    public static Date parse(String dateStr) throws ParseException {
        return parse(dateStr, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 将日期字符串转日期类型。
     *
     * @param dateStr 日期规则字符串，如："2021-11-13 16:09:04"
     * @param formatStr 转换规则，如："yyyy-MM-dd HH:mm:ss"
     * @throws ParseException 转换异常
     * @return 转换后的日期对象
     */
    public static Date parse(String dateStr, String formatStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
        return sdf.parse(dateStr);
    }


    /**
     * 检验是否是闰年。
     *
     * @param year 检验的日期
     * @throws DateHandleException 入参不字符串不符合年份校验
     * @return 是闰年 <code>true</code>,不是闰年 <code>false</code>
     */
    public static boolean checkLeapYear(String year) throws DateHandleException {
        Integer integer = Integer.valueOf(year);
        return checkLeapYear(integer);
    }
    /**
     * 检验是否是闰年。
     *
     * @param year 检验的日期
     * @throws DateHandleException 入参不是年份数字
     * @return 是闰年 <code>true</code>,不是闰年 <code>false</code>
     */
    public static boolean checkLeapYear(Integer year) throws DateHandleException {
        if (RegularUtils.checkYear(year + "")) {
            Calendar calendar = Calendar.getInstance();
            // 将年月日设置为：year年3月1日
            calendar.set(Integer.valueOf(year),2,1);
            // 天数-1
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            return calendar.getActualMaximum(Calendar.DAY_OF_MONTH) == 29;
        } else {
            throw new DateHandleException("入参不可校验为年份：" + year);
        }
    }

}
