package com.qs.commontools.utils.reflex;

/**
 * 反编译工具类
 * @author qiusuo
 * @since 2021-09-09
 */
public class ReflexUtils {

//    private static final String CLAZZ = "class ";
//
//
//    /**
//     * 获取方法名称。
//     *
//     * @param field 通过反射获取到的属性对象
//     * @return 方法的名称
//     * @throws Exception 异常
//     */
//    public static String getMethodName(Field field) throws Exception {
//        return getMethodName(field.getGenericType().toString(),field.getName());
//    }
//
//    /**
//     * 获取方法名称。
//     *
//     * @param fieldType 属性类型字符串
//     * @param fieldName 属性名名称
//     * @return 方法的名称
//     * @throws Exception 异常
//     */
//    public static String getMethodName(String fieldType, String fieldName) throws Exception {
//        String mothodName = getMethodName(fieldName);
//        String result = "";
//        /**
//         * 这里需要说明一下：他是根据拼凑的字符来找你写的getter方法的
//         * 在Boolean值的时候是isXXX（默认使用ide生成getter的都是isXXX）
//         * 如果出现NoSuchMethod异常 就说明它找不到那个gettet方法 需要做个规范
//         */
//        // 如果type是类类型，则前面包含"class "，后面跟类名
//        // 如果是类类型
//        if (fieldType.startsWith(CLAZZ)) {
//            // 拿到该属性的gettet方法
//            result =  "get" + mothodName;
//        }
//
//        // 如果类型是boolean 基本数据类型不一样 这里有点说名如果定义名是 isXXX的 那就全都是isXXX的
//        // 反射找不到getter的具体名
//        if ("boolean".equals(fieldType)) {
//            result = "is" + mothodName;
//        }
//
//        return result;
//
//    }
//
//    /**
//     * 反射根据方法名和Class反射类获取值。
//     *
//     * @param field 需要获取的属性
//     * @param object 需要获取值的实例
//     * @return 获取到的值
//     * @throws NoSuchMethodException 没有这个方法
//     * @throws InvocationTargetException
//     * @throws IllegalAccessException
//     */
//    public static Object getValue(Field field, Object object) throws Exception {
//        String methodName = getMethodName(field);
//        Method m = (Method) object.getClass().getMethod(methodName);
//        return m.invoke(object);
//    }
//
//    /**
//     * 反射根据方法名和Class反射类获取值
//     * @param methodName 获取值的方法名
//     * @param object 需要获取值的实例
//     * @return
//     * @throws NoSuchMethodException
//     * @throws InvocationTargetException
//     * @throws IllegalAccessException
//     */
//    public static Object getValue(String methodName,Object object) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
//        Method m = (Method) object.getClass().getMethod(methodName);
//        return m.invoke(object);
//    }
//
//    /**
//     * 把一个字符串的第一个字母大写、效率是最高的
//     * @param fildeName 名称
//     */
//    public static String getMethodName(String fildeName) throws Exception{
//        byte[] items = fildeName.getBytes();
//        items[0] = (byte) ((char) items[0] - 'a' + 'A');
//        return new String(items);
//    }

    //    /**
//     * 设置参数
//     * @param param Object类型
//     */
//    public void setParam(Object param) {
//        Map<String, Object> map = new HashMap<>(constants.NUM_SIXTEEN);
//        // 获取到该实体类
//        Class<?> clazz = param.getClass();
//        // 获取该实体类的所有属性，返回Field数组
//        Field[] declaredFields = clazz.getDeclaredFields();
//        // 遍历
//        for (Field field: declaredFields) {
//            // 属性类型
//            String fieldType = field.getGenericType().toString();
//            // 属性名称
//            String fieldName = field.getName();
//
//            Object value = null;
//            try {
//                value = ReflexUtils.getValue(field, param);
//            } catch (Exception e) {
//                System.out.println("反射获取属性值异常：");
//                e.printStackTrace();
//            }
//            map.put(fieldName, value);
//        }
//
//        this.setParam(map);
//    }
}
