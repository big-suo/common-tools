package com.qs.commontools.utils.file;

import com.qs.commontools.common.service.ImgService;
import com.qs.commontools.utils.base.Base64StringTool;
import com.qs.commontools.utils.base.StringUtils;

import java.io.*;

/**
 * pdf工具
 * @author qiusuo
 * @since 2021-11-24
 */
public class PdfTool {


    /**
     * pdf文件 转 base64字符串。
     *
     * @param path pdf文件路径
     * @return pdf文件 的 base64字符串
     * @throws IOException 文件不存在
     */
    public static String toBase64(String path) throws IOException {
        return toBase64(new File(path));
    }

    /**
     * pdf文件 转 base64字符串。
     *
     * @param file pdf文件
     * @return pdf文件 的 base64字符串
     * @throws IOException 文件不存在
     */
    public static String toBase64(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException("文件不存在:" + file.getCanonicalPath());
        }
        ImgService imgService = ImgService.getInstance();
        return Base64StringTool.encodeBase64ToStr(imgService.inputStreamToByte(new FileInputStream(file)));
    }

    /**
     * 解base64编码的pdf文件字符串。
     *
     * @param base64Str base64编码的字符串
     * @return pdf文件字节数组
     * @throws FileNotFoundException base64编码的文件字符串无效
     */
    public static byte[] decodeBase64(String base64Str) throws FileNotFoundException {
        if (StringUtils.isBlank(base64Str)) {
            throw new FileNotFoundException("base64编码的文件字符串无效");
        }
        BufferedInputStream bis = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;

        byte[] bytes = Base64StringTool.decodeBase64ToByte(base64Str);
//        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes);
//        bis = new BufferedInputStream(byteInputStream);
        return bytes;
    }
}
