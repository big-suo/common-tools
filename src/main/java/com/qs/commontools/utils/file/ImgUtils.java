package com.qs.commontools.utils.file;

import com.qs.commontools.common.service.ImgService;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * 图片工具类
 * @author qiusuo
 * @since 2021-09-24
 */
public class ImgUtils {
    /**
     * 判断图片base64字符串的文件格式。
     *
     * @param b base64的字节码数组
     * @return 图片的后缀
     */
    public static String checkImageBase64Format(byte[] b) {
        String type = "";
        if (0x424D == ((b[0] & 0xff) << 8 | (b[1] & 0xff))) {
            type = "bmp";
        } else if (0x8950 == ((b[0] & 0xff) << 8 | (b[1] & 0xff))) {
            type = "png";
        } else if (0xFFD8 == ((b[0] & 0xff) << 8 | (b[1] & 0xff))) {
            type = "jpg";
        }
        return type;
    }

    /**
     * 判断图片base64字符串的文件格式。
     *
     * @param b base64的字符串
     * @return 图片的后缀
     */
    public static String checkImageBase64Format(String b) {
       return checkImageBase64Format( org.apache.commons.codec.binary.Base64.decodeBase64(b));
    }



    /**
     * 绘制文本图片。
     *
     * @param text 图片要写的文本
     * @return 文本图片的字节数组
     */
    public static byte[] getImg(String text) {
        ImgService imgService = ImgService.getInstance();
        VerificationTool entity = VerificationTool.getInstance();
        entity.setCode(text);
        entity.setInterLineNum(0);
        entity.setPointRate(0.0f);
        return imgService.draw(entity);
    }

    /**
     * 保存图片。
     *
     * @param inputStream 图片流
     * @param path 想要保存的路径(不包含文件名和后缀)
     * @param name 要保存的文件名称（不带文件后缀）
     * @param suffix 文件后缀（JPG，PDF）
     * @throws IOException 保存异常
     *
     */
    public static void save(InputStream inputStream, String path, String name, String suffix) throws IOException {
        save(inputStreamToByte(inputStream), path, name, path);
    }

    /**
     * 保存图片。
     * @param bytes 图片字节数组流
     * @param path 想要保存的路径(不包含文件名和后缀)
     * @param name 要保存的文件名称（不带文件后缀）
     * @param suffix 文件后缀（JPG，PDF）
     * @throws IOException 保存异常
     */
    public static void save(byte[] bytes,  String path, String name, String suffix) throws IOException {
        FileUtils.save(bytes, path, name, suffix);
    }

    /**
     * InputStream转byte数组。
     *
     * @param inputStream 输入流
     * @throws IOException 转换异常
     * @return byte数组。
     */
    public static byte[] inputStreamToByte(InputStream inputStream) throws IOException {
        return FileUtils.inputStreamToByte(inputStream);
    }

    /**
     * byte数组转InputStream。
     *
     * @param bytes 字节数组
     * @return InputStream
     */
    public static InputStream byteToInputStream(byte[] bytes) {
        return new ByteArrayInputStream(bytes);
    }

    /**
     * BufferedImage对象转InputStream。
     *
     * @param bufferedImage BufferedImage对象
     * @param suffix 后缀，如"JPG"
     * @return InputStream
     */
    public static InputStream bufferedImageToInputStream(BufferedImage bufferedImage,String suffix) {
        ImgService imgService = ImgService.getInstance();
        return imgService.bufferedImageToInputStream(bufferedImage,suffix);
    }

    /**
     * BufferedImage对象转InputStream,默认后缀为jpg。
     *
     * @param bufferedImage BufferedImage对象
     * @return InputStream
     */
    public static InputStream bufferedImageToInputStream(BufferedImage bufferedImage) {
        return bufferedImageToInputStream(bufferedImage,"jpg");
    }

    /**
     * BufferedImage对象转byte数组。
     * <br>BufferedImage对象是文件对象需要文件后缀
     *
     * @param bufferedImage BufferedImage对象
     * @param suffix 文件对象后缀，如"JPG"
     * @return byte数组
     */
    public static byte[] bufferedImageToByte(BufferedImage bufferedImage,String suffix) {
        ImgService imgService = ImgService.getInstance();
        return imgService.bufferedImageToByte(bufferedImage,suffix);
    }

    /**
     * BufferedImage对象转Byte数组,默认后缀为jpg。
     *
     * @param bufferedImage BufferedImage对象
     * @return byte数组
     */
    public static byte[] bufferedImageToByte(BufferedImage bufferedImage) {
        return bufferedImageToByte(bufferedImage, "jpg");
    }



    /**
     * 图片的压缩。
     *
     * @param file 需要被压缩的文件
     * @param width 压缩的宽度像素
     * @param height 压缩的长度像素
     * @return 压缩后的图片的字节数组
     * @throws Exception 压缩异常
     */
    public static byte[] resize(File file, Integer width, Integer height) throws Exception {
        ImgService imgService = ImgService.getInstance();
        BufferedImage bufferedImage = imgService.resize(width, height, file);
        byte[] bytes = bufferedImageToByte(bufferedImage);
        return bytes;
    }

    /**
     * 获得图片宽。
     *
     * @param path 图片路径
     * @return 图片的宽
     * @throws IOException 文件读取异常
     */
    public static Integer getWidth(String path) throws IOException {
        return getWidth(new File(path));
    }

    /**
     * 获得图片宽。
     *
     * @param file 图片对象
     * @return 图片的宽
     * @throws IOException 文件读取异常
     */
    public static Integer getWidth(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException("图片文件不存在:" + file.getCanonicalPath());
        }
        Image img = ImageIO.read(file);
        if (null == img){
            throw new FileNotFoundException("图片文件不存在,或图片由于更改后缀名导致和文件本身属性不一致");
        }
        int width = img.getWidth(null);
        return width;
    }

    /**
     * 获得图片高。
     *
     * @param path 图片路径
     * @throws IOException 文件读取异常
     * @return 图片高
     */
    public static Integer getHeight(String path) throws IOException {
        return getHeight(new File(path));
    }

    /**
     * 获得图片高。
     *
     * @param file 图片对象
     * @throws IOException 文件读取异常
     * @return 图片高
     */
    public static Integer getHeight(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException("图片文件不存在:" + file.getCanonicalPath());
        }
        Image img = ImageIO.read(file);
        if (null == img){
            throw new FileNotFoundException("图片文件不存在,或图片由于更改后缀名导致和文件本身属性不一致");
        }
        int width = img.getHeight(null);
        return width;
    }

//    public static byte[] resize(File file, Integer size) throws Exception {
//        ImgService imgService = ImgService.getInstance();
//        BufferedImage bufferedImage = imgService.resize(file, size);
//        byte[] bytes = bufferedImageToByte(bufferedImage);
//        return bytes;
//    }



}
