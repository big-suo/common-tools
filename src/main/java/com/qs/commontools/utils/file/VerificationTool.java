package com.qs.commontools.utils.file;

import com.qs.commontools.common.service.ImgService;
import com.qs.commontools.utils.base.StringUtils;

import java.awt.*;
import java.io.Serializable;

/**
 * 验证码图片绘制工具
 * @author qiusuo
 * @since 2021-09-27
 */
public class VerificationTool  implements Serializable {
    private static final long serialVersionUID = -832189980895281520L;

    /**
     * 画布的长
     */
    private Integer height;

    /**
     * 画布的宽
     */
    private Integer width;

    /**
     * 字符高低是否随机
     */
    private boolean isRandomLocation;

    /**
     * 需要绘制的验证码
     */
    private String code;

    /**
     * 验证码颜色
     */
    private Color codeColor;

    /**
     * 画布背景
     */
    private Color backgroundColor;

    /**
     * 干扰线数目
     */
    private Integer interLineNum;

    /**
     * 干扰线颜色
     */
    private Color interLineColor;

    /**
     * 干扰线的宽度
     */
    private Float interLineWeight;

    /**
     * 干扰点点的比例
     */
    private float pointRate;

    /**
     * 干扰点点的颜色
     */
    private Color pointColor;

    /**
     * 生成的图片文件后缀
     */
    private String suffix;

    private VerificationTool() {
    }

    /**
     * 获取验证码绘制工具实例对象。
     *
     * @return 验证码绘制工具实例对象
     */
    public static VerificationTool getInstance() {
        VerificationTool tool = new VerificationTool();
        // 设置生成图片的长宽像素
        tool.setHeight(180);
        tool.setWidth(360);
        // 设置绘制的验证码是否随机上下浮动，默认为浮动
        tool.setRandomLocation(true);
        // 默认获取最基础4位验证码
        tool.setCode(StringUtils.getVerification());
        // 默认验证码颜色为黑色
        tool.setCodeColor(Color.BLACK);
        // 默认背景为白色
        tool.setBackgroundColor(new Color(255, 255, 255));
        // 默认干扰线数量为10
        tool.setInterLineNum(10);
        // 默认干扰线颜色灰色
        tool.setInterLineColor(Color.GRAY);
        // 默认干扰线的宽度为10
        tool.setInterLineWeight(10.0f);
        // 默认干扰点点的比例为0.05
        tool.setPointRate(0.05f);
        // 默认干扰点点的颜色
        tool.setPointColor(new Color(56, 51, 51));
        // 默认生成的图片文件后缀为jpg
        tool.setSuffix("jpg");
        return tool;
    }

    /**
     * 获取序列号。
     *
     * @return 序列号
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * 获得生成图片的高。
     * <br>默认180
     *
     * @return 图片的高
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * 设置生成图片的高。
     * <br>默认180
     *
     * @param height 图片的高
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * 获得生成图片的宽度。
     * <br>默认300
     *
     * @return 图片的宽度
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * 设置生成图片的宽度。
     * <br>默认300
     *
     * @param width 图片的宽度
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * 获得绘制的验证码是否随机上下浮动。
     * <br>默认为浮动
     *
     * @return true 上下浮动，false不浮动
     */
    public boolean isRandomLocation() {
        return isRandomLocation;
    }

    /**
     * 设置绘制的验证码是否随机上下浮动。
     * <br>默认为浮动
     *
     * @param randomLocation  true 上下浮动，false不浮动
     */
    public void setRandomLocation(boolean randomLocation) {
        isRandomLocation = randomLocation;
    }

    /**
     * 获得要绘制的验证码。
     *
     * @return 验证码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置要绘制的验证码。
     *
     * @param code 验证码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获得绘制验证码的颜色。
     * <br>默认黑色
     *
     * @return 验证码颜色
     */
    public Color getCodeColor() {
        return codeColor;
    }

    /**
     * 设置绘制验证码的颜色。
     * <br>默认黑色
     *
     * @param codeColor 验证码颜色
     */
    public void setCodeColor(Color codeColor) {
        this.codeColor = codeColor;
    }

    /**
     * 获得图片背景颜色。
     *
     * @return 背景颜色
     */
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * 设置图片背景颜色。
     *
     * @param backgroundColor  背景颜色
     */
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * 获得干扰线的数量。
     *
     * @return 干扰线的数量
     */
    public Integer getInterLineNum() {
        return interLineNum;
    }

    /**
     * 设置干扰线的数量。
     *
     * @param interLineNum 干扰线的数量
     */
    public void setInterLineNum(Integer interLineNum) {
        this.interLineNum = interLineNum;
    }

    /**
     * 获得干扰线的颜色。
     *
     * @return 干扰线的颜色
     */
    public Color getInterLineColor() {
        return interLineColor;
    }

    /**
     * 设置干扰线的颜色。
     *
     * @param interLineColor 干扰线的颜色
     */
    public void setInterLineColor(Color interLineColor) {
        this.interLineColor = interLineColor;
    }

    /**
     * 获得干扰线的宽度。
     *
     * @return 干扰线的宽度
     */
    public Float getInterLineWeight() {
        return interLineWeight;
    }

    /**
     * 设置干扰线的宽度。
     *
     * @param interLineWeight 干扰线的宽度
     */
    public void setInterLineWeight(Float interLineWeight) {
        this.interLineWeight = interLineWeight;
    }

    /**
     * 获得干扰点点的密度，0～1f。
     * 默认0.05f
     *
     * @return 干扰点点的密度
     */
    public float getPointRate() {
        return pointRate;
    }

    /**
     * 设置干扰点点的密度，0～1f,默认0.05f。
     *
     * @param pointRate 干扰点点的密度
     */
    public void setPointRate(float pointRate) {
        this.pointRate = pointRate;
    }

    /**
     * 获得干扰点点的颜色。
     *
     * @return 干扰点点的颜色
     */
    public Color getPointColor() {
        return pointColor;
    }

    /**
     * 设置干扰点的颜色。
     *
     * @param pointColor 干扰点点的颜色
     */
    public void setPointColor(Color pointColor) {
        this.pointColor = pointColor;
    }

    /**
     * 获得生成图片后缀，默认"JPG"。
     *
     * @return 图片后缀
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * 设置生成图片后缀，默认"JPG"。
     *
     * @param suffix  图片后缀
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }


    /**
     * 绘制验证码图片。
     *
     * @return 验证码图片的字节数组
     */
    public byte[] create() {
        ImgService imgService = ImgService.getInstance();
        return imgService.draw(this);
    }


}
